require 'json'
index = []
File.readlines('docs/.vimwiki_tags').each do |line|
  if line.match(/^!_TAG/)
    next
  end
  arr = line.split(/\t/)
  tag = arr[0]
  fn = arr[1]
  link = arr[3]
  fn.sub! '.wiki', '.html'
  arr = link.split('#')
  url = ''
  if arr.size() > 2
    anchor = arr[-1].chomp
    url = fn + '#' + anchor
  end
  title = arr[1]
  url = fn
  if url
    index.push({'title': title, 'tags': tag, 'url': url, 'text': tag})
  end
end

File.open("docs_html/assets/js/tipuesearch/tipuesearch_content.js","w") do |f|
  print 'var tipuesearch = {"pages": ' + index.to_json + '};'
  f.write('var tipuesearch = {"pages": ' + index.to_json + '};')
end
