import os
import json
import sys

if len(sys.argv) < 3:
    exit()

server = sys.argv[1]
path = sys.argv[2]

fh = open('docs/.vimwiki_tags', 'r')
lines = fh.readlines()

index = []
for line in lines:
    if line.startswith('!_TAG'):
        continue
    (tag, fn, lno, link) = line.split('\t')
    fn = fn.replace('.wiki', '.html')
    arr = link.split('#')
    url = ''
    if arr > 1:
        anchor = arr[-1].rstrip('\n')
        url = '%s#%s' % (fn, anchor)
    if url:
        index.append({'title': fn, 'tags': tag, 'url': url, 'text': tag})

fh.close()
print 'Built index %s' % sys.argv

dirname = sys.argv[1]
fh = open('docs_html/tipuesearch/tipuesearch_content.js', 'w')
print 'var tipuesearch = {"pages": %s};' % json.dumps(index)

fh.write('var tipuesearch = {"pages": %s};' % json.dumps(index))
print 'Write Index Count %s' % len(index)
fh.close()

print 'Copying assets'
os.unlink('test/assets')
os.system('cp -r www/assets test')

#print 'rsync -r --delete-after --quiet docs_html/ %s:%s' % (server, path)
#os.system('rsync -r --delete-after --quiet docs_html/ %s:%s' % (server, path))

print 'Done'
