$(document).ready(function () {
    $('table:not(#example)').DataTable();
});

function removeifmain() {
    if ($( "#md-title" ).first().text() === 'index'){
        $( "#md-title" ).remove()
    }
    $( ".tag" ).hide();
    $( "pre" ).html(function() {
        return $(this).attr('data')

    });
    $("pre").addClass("prettyprint");
    $("img").addClass("img-fluid");
    $("img").css("max-width", "100%")
    $( "table" ).wrap( "<div class='table-responsive'></div>" );
    $("h2[id^='Run']").after('<iframe height="800px" width="100%" src="https://repl.it/@gktrishul/Test?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>')
    var srch = ''
    var prefix = ''
    if ($( "h4[id^='related']" ).length) {
        var x = $( "h4[id^='related']" );
        x_str = x.attr("id");
        ar = x_str.split(" ");
        srch = ar[1]
        if (ar.length > 2) {
            prefix = ar[2]
        }
    }
    $( "h4[id^='related']" ).replaceWith("<div class='table-responsive'><table id='example' style='width:100%'></div>");
    if ($("table#example").length) {
        var data1 = [];
        for (i = 0; i < tipuesearch["pages"].length; i++) {
            if (tipuesearch["pages"][i]["tags"] === srch) {
                var url = tipuesearch["pages"][i]["url"]
                var title = tipuesearch["pages"][i]["title"] 
                var arr = url.split("#")
                data1.push(["<a href='" + prefix + arr[0] + "'>" + title + "</a>"]);
            }
        }
        $('#example').DataTable({
            data: data1,
            columns: [ { title: "Pages"} ]
        });
    }
}

removeifmain()

