FROM jekyll/jekyll

RUN apk update
RUN apk add vim
RUN apk add curl
RUN apk add openrc
RUN mkdir -p ~/.vim/autoload ~/.vim/bundle
RUN git clone https://github.com/vimwiki/vimwiki.git ~/.vim/bundle/vimwiki
RUN curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

COPY .vimrc /root/.vimrc
COPY build/build.rb /srv
# Until PR is accepted
COPY build/html.vim /root/.vim/bundle/vimwiki/autoload/vimwiki/html.vim
VOLUME /srv/docs
VOLUME /srv/docs_html
VOLUME /srv/tmpl
VOLUME /srv/code
RUN echo "/usr/bin/vim -c ':VimwikiIndex' -c 'VimwikiAll2HTML!' -c ':VimwikiRebuildTags' -c ':q'" >> /srv/compile
RUN echo "cp -r tmpl/search.html docs_html" >> /srv/compile
RUN echo "cp -r tmpl/assets docs_html" >> /srv/compile
RUN echo "/usr/local/bin/ruby build.rb" >> /srv/compile
WORKDIR /srv
CMD ["/bin/sh", "compile"]
