execute pathogen#infect()
syntax on
filetype plugin indent on

let wiki = {}
let wiki.path = '/srv/docs/'
let wiki.path_html = '/srv/docs_html/'
let wiki.template_path = '/srv/tmpl'
let wiki.template_default = 'index'
let wiki.template_ext = '.html'
let wiki.auto_generate_tags = 1
let g:vimwiki_list=[wiki]

function! VimwikiWikiIncludeHandler(value)
    let str = a:value

    " complete URL
    let url_0 = matchstr(str, vimwiki#vars#get_global('rxWikiInclMatchUrl'))
    if str !~ '\.py'
        return ''
    endif
    " URL parts
    let link_infos = vimwiki#base#resolve_link(url_0)
    let fname = vimwiki#path#current_wiki_file()
    let base =  vimwiki#vars#get_wikilocal('path')
    let rel = vimwiki#path#relpath(fnamemodify(fname, ':h'), link_infos.filename)
    let rel_dir =  vimwiki#vars#get_bufferlocal('subdir')
    let abs_path = base . rel_dir . rel
    echo abs_path
    let lines = readfile(abs_path)
    let data = join(lines, '<br>')
    return '<pre data="' . data . '" > </pre>'

endfunction

