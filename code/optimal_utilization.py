# python 2

class Solution():
    def optimalUtilization(self, input_a, input_b, target):
        closest_match = []
        match = []
        for x in input_a:
            for y in input_b:
                if x[1] + y[1] == target:
                    match.append([x[0], y[0]])
                elif x[1] + y[1] < target:
                    if not closest_match or closest_match[1] < (x[1] + y[1]):
                        closest_match = [[x[0], y[0]], x[1] + y[1]]

        if match:
            return match

        if closest_match:
            return closest_match[0]

        return []


s = Solution()
print s.optimalUtilization([[1, 8], [2, 7], [3, 14]], [[1, 5], [2, 10], [3, 14]], 20)

print s.optimalUtilization([[1, 8], [2, 15], [3, 9]], [[1, 8], [2, 11], [3, 12]], 20)
