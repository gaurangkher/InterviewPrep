# python 2
class Solution(object):
    def calculate_area(self, i, j, matrix):
        width = None
        height = 1
        area = 0
        index = i
        while index >= 0:
            if width is None:
                width = matrix[index][j]
            else:
                width = min(width, matrix[index][j])
            area = max(area, width*height)
            index -= 1
            height += 1

        return area

    def maximalRectangle(self, matrix):
        # Time Complexity is O(N^2M)
        # M is rows N is columns
        # Runtime: 2200 ms, faster than 5.44%
        # Memory Usage: 18.1 MB, less than 97.12%
        max_area = 0
        for i in range(len(matrix)):
            count = 0
            for j in range(len(matrix[i])):
                if matrix[i][j] == "1":
                    count += 1
                    matrix[i][j] = count
                    max_area = max(max_area, self.calculate_area(i,j, matrix))

                else:
                    matrix[i][j] = 0
                    count = 0

        return max_area

s = Solution()
print s.maximalRectangle([
["1","0","1","0","0"],
["1","0","1","1","1"],
["1","1","1","1","1"],
["1","0","0","1","0"]
]) # 6
