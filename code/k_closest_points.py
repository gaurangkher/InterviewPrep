# python2 
class Solution():
    def kClosest(self, points, K):
        # Time Complexity: O(nlog(n))
        # Runtime: 736 ms, faster than 31.57%
        # Memory Usage: 19.1 MB, less than 31.72%
        dists = {}
        for p in points:
            d_square = p[0]**2 + p[1]**2
            dists['%s%s'% (p[0], p[1])] = d_square
        def sorter(x):
            k = '%s%s'% (x[0], x[1])
            return dists[k]
        sorted_dists = sorted(points, key=sorter)
        return sorted_dists[:K]


s = Solution()
print s.kClosest([[1,3],[-2,2]], 1)

