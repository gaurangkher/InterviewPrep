# python2

class UnionFind():
    def __init__(self, nodes):
        self.sets = {}
        self.distinct_sets = 0
        for n in nodes:
            self.distinct_sets += 1
            self.sets[n] = {'parent': n, 'rank': 0}

    def find(self, x):
        if self.sets[x]['parent'] != x:
            self.sets[x]['parent'] = self.find(self.sets[x]['parent'])
        return self.sets[x]['parent']

    def union(self, x, y):
        root_x = self.find(x)
        root_y = self.find(y)

        if root_x == root_y:
            return
        if self.sets[root_x]['rank'] < self.sets[root_y]['rank']:
            self.sets[root_y]['rank'] += 1
            self.sets[root_x]['parent'] = root_y
        else:
            self.sets[root_x]['rank'] += 1
            self.sets[root_y]['parent'] = root_x
        self.distinct_sets -= 1
        #print 'Union %s and %s' % (x, y)

    def is_single_set(self):
        if self.distinct_sets > 1:
            return False
        return True

class Solution():
    def criticalConnections(self, n, connections):
        # Union Find
        critical_connections = []
        for exclude_edge in connections:
            uf = UnionFind(range(1,n+1))
            for edge in connections:
                if edge != exclude_edge:
                    uf.union(edge[0], edge[1])
            if not uf.is_single_set():
                critical_connections.append(exclude_edge)
        return critical_connections


s = Solution()
# 3 -- 4 -- 2
#  \  /
#    1 -- 2
print s.criticalConnections(5, [[1, 2], [1, 3], [3, 4], [1, 4], [4, 5]]) # [1, 4]

# 1    4
# | \ /  \
# |  2    6
# | / \  /
# 3    5
print s.criticalConnections(6, [[1, 2], [1, 3], [2, 3], [2, 4], [2, 5], [4, 6], [5, 6]]) # [2]

#    1        7
#   / \      / \
#  2 - 3 -- 6   8
#      |     \ /
#      4-- 5  9

print s.criticalConnections(9, [[1, 2], [1, 3], [2, 3], [3, 4], [3, 6], [4, 5], [6, 7], [6, 9], [7, 8], [8, 9]])
# [3,4,6]

