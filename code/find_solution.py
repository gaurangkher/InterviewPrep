# python 2
#This is the custom function interface.
#You should not implement it, or speculate about its implementation
class CustomFunction():
    # Returns f(x, y) for any given positive integers x and y.
    # Note that f(x, y) is increasing with respect to both x and y.
    # i.e. f(x, y) < f(x + 1, y), f(x, y) < f(x, y + 1)
    def f(self, x, y):
	return x+y

class Solution(object):
    def findSolution(self, customfunction, z):
        # Time Complexity O(n)
        # Runtime: 24 ms, faster than 84.56% 
        # Memory Usage: 12.7 MB, less than 87.37%
        start = 1
        end = z
        result = []
        while start <= z and end >= 1:
            if customfunction.f(start, end) == z:
                result.append([start, end])
                start += 1
                end -= 1
            elif customfunction.f(start, end) < z:
                start += 1
            else:
                end -= 1
        return result


cf = CustomFunction()
s = Solution()
print s.findSolution(cf, 5) # [[1, 4], [2, 3], [3, 2], [4, 1]]
