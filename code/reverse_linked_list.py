class Solution(object):
    def reverseList(self, head):
        # Recursion
        # Time Complexity O(n)
        # Runtime: 44 ms, faster than 12.59%
        # Memory Usage: 18.4 MB, less than 17.44%
        if not head:
            return head
        if head and head.next is None:
            return head
        h = self.reverseList(head.next)
        head.next.next = head
        head.next = None
        return h

    def reverseListIteration(self, head):
        # Iteration
        # Time Complexity O(n)
        # Runtime: 32 ms, faster than 38.40%
        # Memory Usage: 14.8 MB, less than 62.99%
        if not head:
            return head
        n = None
        while head:
            tmp = head.next
            head.next = n
            n = head
            head = tmp
        return n

from lib.utils import build_linked_list
s = Solution()
ll = build_linked_list([1,2,3,4,5])
print s.reverseList(ll) # 5 -> 4 -> 3 -> 2 -> 1 -> None
print s.reverseListIteration(build_linked_list([1,2,3,4,5]))
