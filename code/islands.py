# python2
class Solution(object):
    # Runtime: 296 ms, faster than 5.02%
    # Memory Usage: 19.8 MB, less than 83.98%
    # Central Idea: Iterate through each unvisited land cell and do DFS if sell is part of island
    # DFS marks cells visited
    # Time Complexity is O(n) since we traverse through each node only once.
    # we ignore visited and water  cells
    def is_visited(self, x):
        if x == 2 or x == '2':
            return True
        return False
    def is_water(self, x):
        if x == 0 or x == '0':
            return True
        return False

    def in_bounds(self, row, column):
        if row < 0 or row >= len(self.grid):
            return False
        if column < 0 or column >= len(self.grid[row]):
            return False
        return True

    def dfs(self, row, column):
        # Check if in bounds
        if self.in_bounds(row, column):

            if self.is_visited(self.grid[row][column]) or self.is_water(self.grid[row][column]):
                return

            # Mark visited
            self.grid[row][column] = 2

            self.dfs(row - 1 , column)
            self.dfs(row + 1, column)
            self.dfs(row, column + 1)
            self.dfs(row, column - 1)
        return
    def numIslands(self, grid):
        self.grid = grid
        islands = 0
        for row in range(len(grid)):
            for column in range(len(grid[row])):
                if not self.is_visited(grid[row][column]) and not self.is_water(grid[row][column]):
                    islands += 1
                    self.dfs(row, column)
        return islands

s = Solution()
grid = [
    [1,1,1,1,0],
    [1,1,0,1,0],
    [1,1,0,0,0],
    [0,0,0,0,0]
]
print s.numIslands(grid)
grid = [
    [1,1,0,0,0],
    [1,1,0,0,0],
    [0,0,1,0,0],
    [0,0,0,1,1]
]
print s.numIslands(grid)

grid = [
    ['1','1','0','0','0'],
    ['1','1','0','0','0'],
    ['0','0','1','0','0'],
    ['0','0','0','1','1']
]
print s.numIslands(grid)
