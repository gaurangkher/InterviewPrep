class DLL():
    def __init__(self, val):
        self.val = val
        self.freq = 0
        self.head = None
        self.tail = None
class LFUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.capacity = capacity
        if not self.capacity:
            self.capacity = 1
        self.d = {}
        self.freq = {}
        self.head = None
        self.tail = None
        self.count = 0

    def _update_freq(self, key):
        dll = self.d[key]['node']
        dll.freq += 1
        if self.freq.has_key(dll.freq+1):
            pass

    def _add_freq(self, key):
        dll = DLL(key)
        self.freq[key] = dll
        if not self.head:
            self.head = dll
            self.tail = dll
        else:
            self.head.head = dll
            dll.tail = self.head
            self.head = dll

    def _evict(self):
        dll = self.head
        self.head = dll.tail
        k = dll.val
        del self.freq[k]
        del self.d[k]
        self.count -= 1

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if self.d.has_key(key):
            self._update_freq(key)
            return self.d[key]
        return -1


    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: None
        """
        if self.d.has_key(key):
            # update key
            self._update_freq(key)
            self.d[key] = value

        else:
            if self.count + 1 > self.capacity:
                self._evict()
            if self.count + 1 <= self.capacity:
                self.d[key] = value
                self._add_freq(key)
                self.count += 1



# Your LFUCache object will be instantiated and called as such:
obj = LFUCache(2)
obj.put(1,2)
obj.put(2,2)
obj.get(1)
print 'H%s T%s' % (obj.head.val, obj.tail.val)
obj.put(3,3)

print obj.get(2)
print obj.get(3)
