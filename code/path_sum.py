# python2
class Solution(object):
    def dfs(self, node, total, path):
        path.append(node.val)
        if node.left is None and node.right is None:
            if total == sum(path):
                return True
        
        l = False
        if node.left:
            l = self.dfs(node.left, total, path)
            if l:
                return l
        r = False
        if node.right:
            r = self.dfs(node.right, total, path)
            if r:
                return r
        if l or r:
            return True
        path.pop()
        return False
    
    def hasPathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: bool
        """
        if not root:
            return False
        return self.dfs(root, sum, [])

from lib.utils import build_tree
t = build_tree([5,4,8,11,None,13,4,7,2,None,None,None,1])
s = Solution()
print s.hasPathSum(t, 22) # True
