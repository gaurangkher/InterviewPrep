# python2
class Solution(object):
    def firstMissingPositive(self, nums):
        # Use dict
	# Time Complexity O(N)
	# Runtime: 36 ms, faster than 25.69%
	# Memory Usage: 12.8 MB, less than 32.68%
	d = {}
        max_n = 0
        for n in nums:
            if max_n < n:
                max_n = n
            d[n] = 1

        missing = max_n
        for i in xrange(1, max_n):
            if not d.has_key(i):
                missing = i
                break

        if missing == max_n:
            missing += 1

        return missing


s = Solution()
print s.firstMissingPositive([1,2,0]) # 3
print s.firstMissingPositive([3,4,-1,1]) # 2 
print s.firstMissingPositive([7,8,9,11,12]) # 1
