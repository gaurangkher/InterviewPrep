#python2
# LeetCode stats
# Runtime: 28 ms, faster than 96.07%
# Memory Usage: 13.7 MB, less than 58.75%
def is_palindrome(string):
    f = 0
    b = len(string) - 1
    while b > f: # While was a better choice since we move both pointers if non-alnum
        if not string[f].isalnum():
            f = f + 1
        elif not string[b].isalnum():
            b = b - 1
        elif string[f].lower() == string[b].lower():
            f = f + 1
            b = b - 1
        else:
            return False

    return True

# Tests
print is_palindrome('A man, a plan, a canal: Panama') # True
print is_palindrome('race a car') # False


# Another Solution
# python2
class Solution:
    # Convert string to upper, new string with only alphanum chars and compare with reverse string
    def isPalindrome(self, s):
        n=len(s)
        s=s.upper()
        res=""
        for i in s:
            if (ord(i)>=65 and ord(i)<=90) or (ord(i)>=48 and ord(i)<=57):
                res=res+i
        return res==res[::-1]
