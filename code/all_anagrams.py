# python2
class Solution(object):
    def findAnagrams(self, string, search_str):
        # Two pointers
        # Time Complexity O(n)
        # Runtime: 96 ms, faster than 79.27%
        # Memory Usage: 13.7 MB, less than 70.69%
        check_d = {}
        match_count = 0
        for c in search_str:
            match_count += 1
            if check_d.has_key(c):
                check_d[c] += 1
            else:
                check_d[c] = 1

        d = {}
        indexes = []
        index = None
        count = 0
        start = 0
        for i, c in enumerate(string):
            if check_d.has_key(c):
                if not count:
                    index = i
                count += 1
                if d.has_key(c):
                    d[c] += 1
                else:
                    d[c] = 1
                if count == match_count and d == check_d:
                    indexes.append(index)
                if count >= match_count:
                    count -= 1
                    if d[string[index]] > 1:
                        d[string[index]] -= 1
                    else:
                        del d[string[index]]
                    index += 1
            else:
                d = {}
                count = 0
                index = None

        return indexes

    def findAnagrams1(self, s, p):
        # Time Complexity O(n)
        # Runtime: 92 ms, faster than 76.12%
        # Memory Usage: 14.7 MB, less than 11.50%
        if len(p) > len(s):
            return []

        d = {}
        plen = len(p)
        for c in p:
            if d.has_key(c):
                d[c] += 1
            else:
                d[c] = 1

        out = []
        start = 0
        d1 = {}
        for i in range(len(s)):
            c = s[i]
            if d1.has_key(c):
                d1[c] += 1
            else:
                d1[c] = 1

            if i - start + 1 == plen:
                if d1 == d:
                    out.append(start)
                oc = s[start]
                if d1[oc] == 1:
                    del d1[oc]
                else:
                    d1[oc] -= 1
                start += 1

        return out


s = Solution()
print s.findAnagrams("abab","ab") # [0,1,2]
print s.findAnagrams("cbaebabacd", "abc") # [0, 6]
print s.findAnagrams1("abab","ab") # [0,1,2]
print s.findAnagrams1("cbaebabacd", "abc") # [0, 6]

