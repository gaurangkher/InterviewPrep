from threading import Event
import threading

class FizzBuzz(object):
    def __init__(self, n):
        self.n = n
        self.i = 1
        self.events = {
            'fizz': Event(),
            'buzz': Event(),
            'fizzbuzz': Event(),
            'pnum': Event(),
        }
        self.events['pnum'].set()

    def _maintenance(self, my_event_key, sub, no_wait=False):

        while self.i <= self.n:
            my_event = self.events[my_event_key]
            if not no_wait:
                print 'Before Print %s E %s Wait %s' % (self.i, my_event_key, no_wait)
                my_event.wait()
                no_wait = False
                my_event.clear()
            if self.i > self.n:
                for v in self.events.values():
                    if not v.isSet():
                        v.set()
            else:
                if my_event_key == 'pnum':
                    sub(self.i)
                else:
                    sub()
                self.i += 1
                e_k = self.get_event(self.i)
                print ' OldK %s NewK %s H %s' % (my_event_key, e_k, self.i )
                if e_k == my_event_key:
                    no_wait = True
                else:
                    self.events[e_k].set()
                    no_wait = False
                my_event_key = e_k
        for v in self.events.values():
            if not v.isSet():
                v.set()

    def get_event(self, i):
        if i % 3 == 0 and i % 5 != 0:
            return 'fizz'
        elif i % 3 != 0 and i % 5 == 0:
            return 'buzz'
        elif i % 3 == 0 and i % 5 == 0:
            return 'fizzbuzz'
        else:
            return 'pnum'

    # printFizz() outputs "fizz"
    def fizz(self, printFizz):
        """
        :type printFizz: method
        :rtype: void
        """
        self._maintenance('fizz', printFizz)
    	return

    # printBuzz() outputs "buzz"
    def buzz(self, printBuzz):
        """
        :type printBuzz: method
        :rtype: void
        """
        self._maintenance('buzz', printBuzz)
    	return

    # printFizzBuzz() outputs "fizzbuzz"
    def fizzbuzz(self, printFizzBuzz):
        """
        :type printFizzBuzz: method
        :rtype: void
        """
        self._maintenance('fizzbuzz', printFizzBuzz)
        return

    # printNumber(x) outputs "x", where x is an integer.
    def number(self, printNumber):
        """
        :type printNumber: method
        :rtype: void
        """
        self._maintenance('pnum', printNumber,no_wait=True)
        return

# Test Runner
class myThread (threading.Thread):
    def __init__(self, fb, printer):
        threading.Thread.__init__(self)
        self.fb = fb
        self.printer = printer

    def run(self):
        def print_f():
            print 'fizz'
        def print_b():
            print 'buzz'
        def print_fb():
            print 'fizzbuzz'
        def print_n(i):
            print i
        if self.printer == 'fizz':
            self.fb.fizz(print_f)
        elif self.printer == 'buzz':
            self.fb.buzz(print_b)
        elif self.printer == 'fizzbuzz':
            self.fb.fizzbuzz(print_fb)
        else:
            self.fb.number(print_n)

fb = FizzBuzz(15)
t1 = myThread(fb, 'fizz')
t2 = myThread(fb, 'buzz')
t3 = myThread(fb, 'fizzbuzz')
t4 = myThread(fb, 'number')
t1.start()
t2.start()
t3.start()
t4.start()

