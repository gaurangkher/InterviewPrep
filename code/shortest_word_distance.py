# python 2

class Solution(object):
    def shortestDistance(self, words, word1, word2):
        # Time Complexity is O(n)
        # Runtime: 48 ms, faster than 84.64% 
        # Memory Usage: 17.5 MB, less than 27.12% 
        w1  = None
        w2 = None
        min_d = None
        for i,w in enumerate(words):
            if w == word1:
                w1 = i
            elif w == word2:
                w2 = i
            if w1 is not None and w2 is not None:
                if min_d is None:
                    min_d = abs(w1-w2)
                else:
                    min_d = min(min_d, abs(w1-w2))

        return min_d

s = Solution()
print s.shortestDistance(["practice", "makes", "perfect", "coding", "makes"], "coding", "practice") # 3


