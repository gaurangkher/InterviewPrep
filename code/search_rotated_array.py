# python 2
class Solution(object):
    def search(self, nums, target):
        # Binary Search , but account for rotated condition
        # The condition is :
        # if mid < start: (Rotated)
        #   Target between mid and end
        #   OR target is between start and mid
        # Similar 2 conditions when mid > start
        # Runtime: 24 ms, faster than 92.17%
        # Memory Usage: 12.9 MB, less than 74.92%
        # Time Complexity: O(log(n))
        not_found = -1
        if not nums:
            return not_found
        start = 0
        end = len(nums) - 1
        if start == end:
            if nums[start] == target:
                return start
            return not_found

        mid = (end - start) / 2
        while start <= end:
            if nums[mid] == target:
                return mid
            if nums[mid] < nums[start]:
                if target <= nums[end] and target >= nums[mid]:
                    start = mid + 1
                else:
                    end = mid - 1
            else:
                if target >= nums[start] and target <= nums[mid]:
                    end = mid - 1
                else:
                    start = mid + 1
            mid = start + (end - start) / 2
        return -1
    
    def search1(self, nums):
        # Same as search, but added condition for when mid == start
        # BEcause of duplicates, we keep 
        # Runtime: 84 ms, faster than 5.63% 
        # Memory Usage: 13 MB, less than 42.81% 

        not_found = -1
        if not nums:
            return not_found
        start = 0
        end = len(nums) - 1
        if start == end:
            if nums[start] == target:
                return start
            return not_found

        mid = (end - start) / 2
        while start <= end:
            if nums[mid] == target:
                return mid
            if nums[mid] < nums[start]:
                if target <= nums[end] and target >= nums[mid]:
                    start = mid + 1
                else:
                    end = mid - 1
            else:
                if target >= nums[start] and target <= nums[mid]:
                    end = mid - 1
                else:
                    start = mid + 1
            mid = start + (end - start) / 2
        return -1



s = Solution()
print s.search([4,5,6,7,0,1,2], 0) # 4
print s.search([4,5,6,7,0,1,2], -1) # -1
print s.search([1,3,5], 3)

# If sorted array has duplicates
print s.search1([4,5,6,0,0,1,2], 0) # 4
print s.search1([4,5,6,7,0,1,2], -1) # -1
print s.search1([1,3,1,1,1], 3)

