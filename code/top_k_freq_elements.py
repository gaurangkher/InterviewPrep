# python2
import heapq
class Solution(object):
    def topKFrequent(self, nums, k):
        # Hash of freq of elements
        # Heap to get k largest
        # Runtime: 152 ms, faster than 22.84%
        # Memory Usage: 16.4 MB, less than 19.69% 
        d = {}
        for n in nums:
            d.setdefault(str(n), 0)
            d[str(n)] += 1
        def sorter(k):
            return d[k]
        ret = heapq.nlargest(k, d.keys(), key=sorter)
        return ret

s = Solution()
print s.topKFrequent([1,1,1,2,2,3], 2) # ['1', '2']

print s.topKFrequent([1], 1) # ['1']
