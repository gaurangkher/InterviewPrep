#python 2
class Node():
    def __init__(self, name):
        self.name = name
class File(Node):
    def __init__(self, name):
        Node.__init__(self, name)
        self.content = ''
class Dir(Node):
    def __init__(self, name):
        Node.__init__(self, name)
        self.children = {}

class FileSystem(object):

    def __init__(self):
        self.root = Dir('/')

    def _get_root_path(self, path):
        if path.startswith('/'):
            return path[1:]

    def _get_path(self, path):
        node = self.root
        if not path:
            return (node, None)
        parts = path.split('/')
        obj = parts.pop()
        for part in parts:
            if node.children.has_key(part):
                node = node.children[part]
            else:
                raise Exception('Path not found')

        return (node, obj)

    def ls(self, path):
        """
        :type path: str
        :rtype: List[str]
        """
        p = self._get_root_path(path)

        (parent, name) = self._get_path(p)
        if name is None:
            return sorted(parent.children.keys())
        obj = parent.children[name]
        if obj.__class__.__name__ == 'Dir':
            return sorted(obj.children.keys())
        else:
            return [name]


    def mkdir(self, path):
        """
        :type path: str
        :rtype: None
        """
        p = self._get_root_path(path)
        parts = p.split('/')
        self._mkdir(parts)

    def _mkdir(self, parts):
        node = self.root
        for part in parts:
            if node.children.has_key(part):
                node = node.children[part]
            else:
                n = Dir(part)
                node.children[part] = n
                node = n
        return node

    def addContentToFile(self, filePath, content):
        """
        :type filePath: str
        :type content: str
        :rtype: None
        """
        p = self._get_root_path(filePath)
        parts = p.split('/')
        fn = parts.pop()
        node = self._mkdir(parts)
        if node.children.has_key(fn):
            f_obj = node.children[fn]
            f_obj.content += content
        else:
            f_obj = File(fn)
            f_obj.content = content
            node.children[fn] = f_obj

    def readContentFromFile(self, filePath):
        """
        :type filePath: str
        :rtype: str
        """
        p = self._get_root_path(filePath)
        parts = p.split('/')
        fn = parts.pop()
        node = self._mkdir(parts)
        if node.children.has_key(fn):
            f_obj = node.children[fn]
            return f_obj.content


# Your FileSystem object will be instantiated and called as such:
obj = FileSystem()
print obj.ls('/') # []
print obj.mkdir('/a/b/c') # None
print obj.addContentToFile('/a/b/c/d', "hello") # none
print obj.ls('/') # ['a']
print obj.readContentFromFile('/a/b/c/d') # "hello"

