# python 2
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Codec:
    # Serrialize and Deserilize in O(n)
    # Runtime: 200 ms, faster than 23.56% 
    # Memory Usage: 22.9 MB, less than 41.76% 
    def serialize(self, root):
        q = [root]
        arr = []
        while q:
            node = q.pop(0)
            if node is None:
                arr.append(str(None))
            else:
                arr.append(str(node.val))
                q.append(node.left)
                q.append(node.right)
        return '->'.join(arr)

    def add_node(self, val):
        if val == 'None':
            return None
        return TreeNode(int(val))

    def deserialize(self, data):
        arr = data.split('->')
        if arr == 'None':
            return None
        root = self.add_node(arr.pop(0))
        node_arr = [root]
        while arr:
            p = node_arr.pop(0)
            #print p.val
            l = arr.pop(0)
            p.left = self.add_node(l)
            if p.left is not None:
                node_arr.append(p.left)
            r = arr.pop(0)
            p.right = self.add_node(r)
            if p.right is not None:
                node_arr.append(p.right)

        #print self.serialize(root)
        return root

# Your Codec object will be instantiated and called as such:
#codec = Codec()
#codec.deserialize(codec.serialize(root))
