# python 2
# Definition for an Interval.
class Interval(object):
    def __init__(self, start=None, end=None):
        self.start = start
        self.end = end
    def __repr__(self):
        return str((self.start, self.end))

class Solution(object):
    def employeeFreeTime(self, schedule):
	# Time Complexity O(nlog(n))
        # Runtime: 64 ms, faster than 100.00%
        # Memory Usage: 16.3 MB, less than 21.98%

        sorted_schedule = []
        for s in schedule:
            sorted_schedule.extend(s)

        def sorter(k):
            return k.start

        sorted_schedule.sort(key=sorter)

        ret = []
        merged = [sorted_schedule[0]]
        for i in range(1,len(sorted_schedule)):
            s = sorted_schedule[i]
            # Merge
            if s.start <= merged[-1].end:
                merged[-1].end = max(merged[-1].end, s.end)
            else:
                ret.append(Interval(start=merged[-1].end, end=s.start))
                merged.append(s)

        return ret

s = Solution()
schedule = [[Interval(1,2), Interval(5,6)], [Interval(1,3)], [Interval(4,10)]]
print s.employeeFreeTime(schedule) # [(3,4)] 
