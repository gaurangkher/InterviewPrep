# python 2
class Solution(object):
    def reverseParentheses(self, s):
        # Time Complexity O(N^2)
        # Runtime: 16 ms, faster than 88.89%
        # Memory Usage: 13.3 MB, less than 33.54%
        out = ''
        stack = []
        for c in s:
            if c == ')':
                rev = ''
                while stack and stack[-1] != '(':
                    p = stack.pop()
                    rev += p[::-1]
                if stack and stack[-1] == '(':
                    stack.pop()
                if rev:
                    stack.append(rev)
            else:
                stack.append(c)
        
        out = "".join(stack)
        return out

s = Solution()
print s.reverseParentheses("(ed(et(oc))el)") # leetcode
print s.reverseParentheses("a(bcdefghijkl(mno)p)q") # apmnolkjihgfedcbq
print s.reverseParentheses("(u(love)i)") # iloveu
