# python2
class Solution(object):
    def hammingWeight(self, n):
        """
        :type n: int
        :rtype: int
        """
        res = 0
        while n:
            res += n&1
            n /= 2

        return res

s = Solution()
print s.hammingWeight(int("0000000000000000000000000000101", 2))
