#python2
class Solution(object):
    def twoSum(self, nums, target):
        # Use hash
        # Runtime: 32 ms, faster than 94.29%
        # Memory Usage: 14.1 MB, less than 26.74%
        index = {}
        for i in xrange(len(nums)):
            key = target - nums[i]
            if index.has_key(key):
                return [index[key], i]
            index[nums[i]] = i
        return []

