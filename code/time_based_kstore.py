# python2
class TimeMap(object):

    def __init__(self):
        # Runtime: 772 ms, faster than 32.94%
        # Memory Usage: 69.9 MB, less than 12.89%
        self.d = {}

    def bs(self, stamps, ts):
        s = 0
        e = len(stamps) - 1
        if s == e:
            if ts < stamps[s]:
                return None
            return stamps[s]
        while s < e:
            if s+1 == e:
                if ts >= stamps[e]:
                    return stamps[e]
                elif ts < stamps[s]:
                    return None
                return stamps[s]

            m = s + (e-s)/2
            if ts < stamps[m]:
                e = m - 1
            elif ts > stamps[m]:
                s = m
            else:
                return stamps[m]

        return None

    def set(self, key, value, timestamp):
        #  Time Complexity O(1)
        if self.d.has_key(key):
            self.d[key]['stamps'].append(timestamp)
            self.d[key]['vals'][timestamp] = value
        else:
            self.d[key] = {'stamps': [timestamp], 'vals': {timestamp: value}}

    def get(self, key, timestamp):
        # Time Complexity log(n)
        if not self.d.has_key(key):
            return ""
        else:
            ts = self.bs(self.d[key]['stamps'], timestamp)
            if ts is not None:
                return self.d[key]['vals'][ts]
            else:
                return ""

tm = TimeMap()
print tm.set('foo', 'bar', 1) # None
print tm.get('foo', 1) # bar
print tm.get('foo', 3) # bar
print tm.set('foo', 'bar2', 4) # None
print tm.get('foo', 4) # bar2
print tm.get('foo', 5) # bar2
print tm.get('foo', 2) # bar



