# python2
class Solution(object):
    def _build_time(self, h, m):
        ret = ''
        if h < 10:
            ret = '0%s' % h
        else:
            ret = str(h)
        if m < 10:
            ret = '%s:0%s' % (ret,m)
        else:
            ret = '%s:%s' % (ret,m)
        return ret

    def nextClosestTime(self, time):
        # Convert to minutes and count to 24 hrs
        # Time Complexity O(n)
        # Runtime: 24 ms, faster than 61.20%
        # Memory Usage: 12.7 MB, less than 47.00%
        l = 24*60
        digits = {}
        h, m = time.split(':')
        for c in "%s%s" % (h,m):
            digits[int(c)] = 1
        h = int(h)
        m = int(m)
        i = 1
        while i <= l:
            if h == 23 and m == 59:
                m = 0
                h = 0
            elif m == 59:
                m = 0
                h += 1
            else:
                m += 1

            check = '%s%s' % (str(h),str(m))
            if h < 10 or m < 10:
                check += '0'

            found = False
            for c in check:
                if digits.has_key(int(c)):
                    found = True
                else:
                    found = False
                    break
            if found:
                return self._build_time(h,m)
            i += 1

        return 0

s = Solution()
print s.nextClosestTime("19:34") # 19:39
