class Solution(object):
    def isSubsequence(self, s, t):
        # Time Complexity is O(T) 
        # T is target length
        # Runtime: 12 ms, faster than 98.39%
        # Memory Usage: 12.9 MB, less than 75.20%
        si = 0
        ti = 0
        while si < len(s) and ti < len(t):
            if s[si] == t[ti]:
                si += 1
            ti += 1
        if si == len(s):
            return True
        return False

s = Solution()
print s.isSubsequence("abc", "ahbgdc") # True
print s.isSubsequence("axc", "ahbgdc") # False

