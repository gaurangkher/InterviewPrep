# python2
class Solution():
    def longestNonDecreasing(self, arr):
        if not arr:
            return 0
        if len(arr) == 1:
            return 1
        start = 0
        lnd = 0
        prev = arr[0]
        for i in range(1, len(arr)):
            if arr[i] >= prev:
                lnd = max(lnd, i-prev + 1)
                prev = arr[i]
            else:
                start = i

        return lnd

s = Solution()
print s.longestNonDecreasing([5, 2, 4, 3, 1, 6])


