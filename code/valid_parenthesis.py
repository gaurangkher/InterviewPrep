class Solution(object):
    def isValid(self, s):
        # Time Complexity O(n)
        # Runtime: 28 ms, faster than 34.47%
        # Memory Usage: 12.7 MB, less than 94.28%
        d = {
            ')': '(',
            '}': '{',
            ']': '['
        }
        stack = []
        for c in s:
            if c in d.values():
                stack.append(c)
            elif c in d.keys():
                if stack and d[c] == stack[-1]:
                    stack.pop()
                else:
                    stack.append(c)
        if stack:
            return False
        return True
