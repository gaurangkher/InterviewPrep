# python 2
class Solution(object):
    def climbStairs(self, n):
        # Time Complexity O(n)
	# DP[i] = DP[i-1] + DP[i-2]
	# Runtime: 20 ms, faster than 55.95%
	# Memory Usage: 12.9 MB, less than 21.12%
	if not n:
            return 0
        dp = [1,2]
        if n == 1:
            return dp[n-1]
        if n == 2:
            return dp[n-1]
        for i in range(3,n+1):
            dp.append(dp[-1] + dp[-2])
        return dp[-1]

s = Solution()
print s.climbStairs(4) # 5
