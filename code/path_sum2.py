# python 2
class Solution(object):
    def dfs(self, node, total, path, ans):
        #print 'Node %s path %s' % (node.val, path)
        path.append(node.val)
        if node.left is None and node.right is None:
            if total == sum(path):
                ans.append(path[:])
                #print ans
                return ans

        if node.left:
            ans = self.dfs(node.left, total, path, ans)
            path.pop()
        if node.right:
            ans = self.dfs(node.right, total, path, ans)
            path.pop()

        return ans

    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: bool
        """
        ans = []
        if not root:
            return ans
        return self.dfs(root, sum, [], ans)

from lib.utils import build_tree
t = build_tree([5,4,8,11,None,13,4,7,2,None,None,5,1])
s = Solution()
print s.pathSum(t, 22) # [[5, 4, 11, 2], [5, 8, 4, 5]] 
