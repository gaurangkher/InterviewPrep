# python 2
class Solution(object):
    def traverse(self, pt, grid, index):
        x,y = pt
        if x >= 0 and x < len(grid) and y >= 0 and y < len(grid[x]):
            if grid[x][y] == 'E' or grid[x][y] == '0':
                return [x,y]
        return []


    def dfs(self, point, grid):
        i, j = point
        count = 0
        directions = [[i+1, j], [i-1,j], [i, j+1], [i, j-1]]
        for index, d in enumerate(directions):
            x,y = d
            valid = self.traverse(d, grid, index)
            while valid:
                if grid[valid[0]][valid[1]] == 'E':
                    count += 1
                if index == 0:
                    x = x +1
                elif index == 1:
                    x = x - 1
                elif index == 2:
                    y = y + 1
                else:
                    y = y - 1

                valid = self.traverse([x,y], grid, index)
        return count

    def maxKilledEnemies(self, grid):
        # DFS Time Complexity O(n^2)
        # Runtime: 880 ms, faster than 9.59%
        # Memory Usage: 22.2 MB, less than 91.78% 
        count = 0
        max_pos = []
        max_kills = 0
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if grid[i][j] == '0':
                    count = self.dfs([i, j], grid)
                    if count > max_kills:
                        max_pos = (i,j)
                        max_kills = count

        return max_kills

s = Solution()
print s.maxKilledEnemies([["0","E","0","0"],["E","0","W","E"],["0","E","0","0"]]) # 3

