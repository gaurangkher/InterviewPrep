# python 2
class Solution(object):
    def maxLevelSum(self, root):
        # BFS
        # Time Complexity O(n)
        # Runtime: 344 ms, faster than 85.99%
        # Memory Usage: 21.1 MB, less than 48.35%
	if not root:
            return 0
        level = 0
        m_level = 1
        lsum = 0
        q = [root]
        curr_sum = 0
        new_q = []
        while q:
            node = q.pop()
            curr_sum += node.val
            if node.left:
                new_q.append(node.left)
            if node.right:
                new_q.append(node.right)
            if not q:
                level += 1
                if curr_sum > lsum:
                    lsum = curr_sum
                    m_level = level
                curr_sum = 0
                q = new_q
                new_q = []

        return m_level

from lib.utils import build_tree
s = Solution()
print s.maxLevelSum(build_tree([1,7,0,7,-8,None,None])) # 2
