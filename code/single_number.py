# python2
class Solution(object):
    def singleNumber(self, nums):
        # Runtime: 116 ms, faster than 34.72% 
	# Memory Usage: 15.2 MB, less than 16.59%
        # Time Complexity O(n), Space Complexity O(n)
	d = {}
        for n in nums:
            if d.has_key(n):
                del d[n]
            else:
                d[n] = 1
        if d:
            return d.keys()[0]
        return None

    def singleNumberMath(self, nums):
        # 2* (a+b+c) - (a+a+b+b+c) = c
        # Runtime: 136 ms, faster than 27.02%
        # Memory Usage: 14.9 MB, less than 34.08%
        # Time Complexity O(n), Space Complexity O(n)
        s = 2*sum(set(nums))
        return s - sum(nums)


    def singleNumberXor(self, nums):
        # A XOR A = 0, A XOR 0 = A
        # A XOR B XOR C XOR A XOR B = C
        # Runtime: 104 ms, faster than 40.82%
        # Memory Usage: 14.6 MB, less than 59.64%
        s = 0
        for n in nums:
            s ^= n

        return s

s = Solution()
print s.singleNumber([2,2,1])
print s.singleNumberMath([2,2,1])
print s.singleNumberXor([2,2,1])

print s.singleNumber([4,1,2,1,2])
print s.singleNumberMath([4,1,2,1,2])
print s.singleNumberXor([4,1,2,1,2])
