# python2
class Solution(object):
    def is_valid(self, x, y, grid):
        if not grid:
            return False

        if x >= 0 and x < len(grid):
            if y >= 0 and y < len(grid[x]):
                if grid[x][y] == 1:
                    return True
        return False

    def dfs(self, x, y, grid, path):

        if not self.is_valid(x, y, grid):
            path.pop()
            return path
        grid[x][y] = 2
        path.append(1)
        path = self.dfs(x+1, y, grid, path)
        path.append(2)
        path = self.dfs(x-1, y, grid, path)
        path.append(3)
        path = self.dfs(x, y+1, grid, path)
        path.append(4)
        path = self.dfs(x, y-1, grid, path)
        path.append(0)
        return path

    def numDistinctIslands(self, grid):
        # DFS , with path signature
        # Time Complexity O(N)
        # Runtime: 392 ms, faster than 7.24% 
        # Memory Usage: 14.4 MB, less than 72.97% 
        paths = {}
        for x in range(len(grid)):
            for y in range(len(grid[x])):

                path = self.dfs(x, y, grid, [0])
                #print path
                if path:
                    #print grid
                    p = ''
                    for p1 in path:
                        p += str(p1)

                    paths[p] = 1

        return len(paths.keys())


s = Solution()
grid = [
    [1,1,0],
    [0,1,1],
    [0,0,0],
    [1,1,1],
    [0,1,0]
]
print s.numDistinctIslands(grid) # 2

grid = [
    [1, 1, 0, 0, 0 ],
    [1, 1, 0, 0, 0 ],
    [0, 0, 0, 1, 1 ],
    [0, 0, 0, 1, 1 ],
]
print s.numDistinctIslands(grid) # 1
