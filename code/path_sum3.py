# python2
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution(object):
    def __init__(self):
        self.total = 0
    def check(self, path):
        t = 0
        cnt = 0
        for i in range(len(path)-1, -1, -1):
            t += path[i]
            if t == self.total:
                #print path[i:]
                cnt += 1
        return cnt

    def dfs(self, node, path, count):
        if not node:
            return count
        #print 'P %s' % path
        path.append(node.val)

        count += self.check(path)

        if node.left:
            count = self.dfs(node.left, path, count)
            path.pop()
        if node.right:
            count = self.dfs(node.right, path, count)
            path.pop()
        return count

    def pathSum(self, root, sum):
        # DFS
        # Time Complexity O(n)
        # Runtime: 240 ms, faster than 56.05%
        # Memory Usage: 13.7 MB, less than 89.99%
        self.total = sum
        count = 0
        count = self.dfs(root, [], count)
        return count

from lib.utils import build_tree
s = Solution()
t = build_tree([10,5,-3,3,2,None,11,3,-2,None,1])
print s.pathSum(t, 8) # 3
