# python2

class Solution():
    # Time Complexity O(n)
    # Runtime: 44 ms, faster than 43.31% 
    # Memory Usage: 12.8 MB, less than 45.38% 
    def __init__(self):
        self.graph = []
        self.fresh_count = 0

    def is_valid(self, r, c):
        if r < 0 or r >= len(self.graph):
            return False
        if c < 0 or c >= len(self.graph[r]):
            return False
        if self.graph[r][c] == 2 or self.graph[r][c] == 0:
            return False
        return True

    def infect(self, z):
        r = z[0]
        c = z[1]
        new_rotten = []
        for cell in [(r+1,c), (r-1,c), (r, c+1), (r, c-1)]:
            if self.is_valid(cell[0], cell[1]):
                self.graph[cell[0]][cell[1]] = 2
                new_rotten.append(cell)
                self.fresh_count -= 1
        return new_rotten

    def orangesRotting(self, grid):
        self.graph = grid
        hour = 0
        rotten = []
        for row in xrange(len(self.graph)):
            for col in xrange(len(self.graph[row])):
                if self.graph[row][col] == 1:
                    self.fresh_count += 1
                if self.graph[row][col] == 2:
                    rotten.append((row,col))

        while self.fresh_count > 0 and len(rotten) > 0:
            hour += 1
            new_rotten = []
            for z in rotten:
                new_rotten.extend(self.infect(z))
            rotten = new_rotten

        if self.fresh_count > 0:
            return -1
        return hour


s = Solution()
graph = [
    [2,1,1],
    [1,1,0],
    [0,1,1]
]
print s.orangesRotting(graph)

graph = [
    [2,1,1],
    [0,1,1],
    [1,0,1]
]
print s.orangesRotting(graph)
