# python2
class Solution(object):
    def reverse(self, x):
        # Time Complexity is O(log10(x)) 
        # Because x has log x base 10 digits
        # Runtime: 20 ms, faster than 82.97% 
        # Memory Usage: 13 MB, less than 5.01%
        ret_int = 0
        is_neg = False
        if x < 0:
            is_neg = True
            x = x * -1

        stack = []

        while x >= 10:
            rem = x % 10
            x /= 10
            stack.append(rem)
        stack.append(x)

        order = 0
        while stack:
            ret_int += int(stack.pop() * pow(10, order))
            order += 1

        if is_neg:
            ret_int *= -1

        if 2**31-1 > ret_int > -2**31:
            return ret_int
        return 0

s = Solution()
print s.reverse(123) # 321
print s.reverse(-435) # -534
print s.reverse(1223456789) # 0
