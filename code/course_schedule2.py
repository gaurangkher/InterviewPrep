# python2
import enum
class VStatus(enum.Enum):
    UnVisited = 0
    Visited = 1
    Done = 2
class Node():
    def __init__(self, val):
        self.val = val
        self.children = []
        self.visited = VStatus.UnVisited # 1=visited , 2=done

class Solution(object):
    def __init__(self):
        self.cycle = False
    def dfs(self, node, edges, courses, path):
        if node.visited == VStatus.Done:
            return path
        node.visited = VStatus.Visited
        if node.val in edges.keys():
            for n in edges[node.val]:
                c = courses[n]
                node.children.append(c)
                #print 'Node %s C %s visited %s' % (node.val, c.val, c.visited)
                if c.visited == VStatus.Visited: # Cycle
                    self.cycle = True
                    return []
                elif c.visited == VStatus.UnVisited:
                    self.dfs(c, edges, courses, path)

        if self.cycle:
            return []
        else:
            path.append(node.val)
            node.visited = VStatus.Done
            return path

    def findOrder(self, numCourses, prerequisites):
        # DFS (postorder) Topological Sort
        # Time Complexity O(n)
        # Runtime: 264 ms, faster than 11.85%
        # Memory Usage: 16.7 MB, less than 5.04%
	self.cycle = False
        edges = {}
        if not prerequisites:
            return range(numCourses)
        for e in prerequisites:
            if edges.has_key(e[0]):
                edges[e[0]].append(e[1])
            else:
                edges[e[0]] = [e[1]]

        courses = {}
        for k in range(numCourses):
            courses[k] = Node(k)

        ret = []
        for edge in edges.keys():
            ret = self.dfs(courses[edge], edges, courses,ret)
            #print ret
            if not ret:
                return ret

        for c in courses.keys():
            course = courses[c]
            if course.visited == VStatus.UnVisited:
		ret = [c] + ret
        return ret

s = Solution()
print s.findOrder(3, [[0,1],[0,2],[1,2]]) # [2, 1, 0]
print s.findOrder(2, [[0,1], [1,0]]) # []
print s.findOrder(4, [[1,0],[2,0],[3,1],[3,2]]) # [0, 1, 2, 3]
print s.findOrder(2, [[1,0]]) # [0,1]
print s.findOrder(3, [[1,0]]) # [2,0,1]
