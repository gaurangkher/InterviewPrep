# python 2
class Solution(object):
    def minPathSum(self, grid):
        # Time Complexity O(m*n)
        # Runtime: 72 ms, faster than 97.11%
        # Memory Usage: 14.7 MB, less than 19.88%
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                new_val = None
                if i-1 >= 0:
                    new_val = grid[i-1][j]
                if j-1 >= 0:
                    l = grid[i][j-1]
                    if new_val is not None:
                        new_val = min(l, new_val)
                    else:
                        new_val = l
                if new_val is not None:
                    grid[i][j] += new_val

        return grid[-1][-1]

s = Solution()
print s.minPathSum([
    [1,3,1],
    [1,5,1],
    [4,2,1]
]) # 7
