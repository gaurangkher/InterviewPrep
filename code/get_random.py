# python2
import random
class RandomizedSet(object):
    # Runtime: 332 ms, faster than 31.94%
    # Memory Usage: 17.3 MB, less than 80.00%
    def __init__(self):
        #Initialize your data structure here.
        self.d = {}

    def insert(self, val):
        # Inserts a value to the set. 
        # Returns true if the set did not already contain the specified element.
        if self.d.has_key(val):
            return False
        self.d[val] = True
        return True

    def remove(self, val):
        # Removes a value from the set. 
        # Returns true if the set contained the specified element.
        if self.d.has_key(val):
            del self.d[val]
            return True
        return False


    def getRandom(self):
        # Get a random element from the set.
        arr = self.d.keys()
        return arr[int(random.random()*len(arr))]


# Your RandomizedSet object will be instantiated and called as such:
obj = RandomizedSet()
print obj.insert(1) # True
print obj.remove(2) # False
print obj.insert(2) # True
print obj.getRandom() # 1 or 2
print obj.remove(1) # True
print obj.insert(2) # False
print obj.getRandom() # 2
