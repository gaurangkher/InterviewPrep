# python 2

class Solution():
    def longestSubarrMid(elf, arr):
        start = 0
        end = 0
        mlen = 0
        mid = -1
        for i in range(len(arr)):
            if arr[i] != 0:
                start = i
            l = i - start + 1
            if l > mlen:
                mlen = l
                mid = start + float(mlen/2)
            print 'S %s i %s mid %s' % (start, i, mid)
        return mid

s = Solution()
print s.longestSubarrMid([0,1,0,0,0,1,0]) # 3
print s.longestSubarrMid([1,0,0,0,1,1,0,0,0,0,0,1])
