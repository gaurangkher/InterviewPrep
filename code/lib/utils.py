# Utilities for Testing etc

# Linked List class

class LinkedList(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __repr__(self):
        s = ''
        if self.val:
            s = '%s -> %s' % (self.val, str(self.next)) 
        return s
# Build a LInked List from and Array
def build_linked_list(arr, cls=None):
    val = arr.pop(0)
    obj = LinkedList(val)
    if cls:
        obj = cls(val)
    root = obj
    n = root
    for e in arr:
        obj = LinkedList(e)
        if cls:
            obj = cls(e)
        n.next = obj
        n = n.next
    return root


class TreeNode():
    def __init__(self, x): 
        self.val = x 
        self.left = None
        self.right = None
    def __repr__(self):
        r = [self]
        arr = []
        while r:
            n = r.pop(0)
            if n is None:
                arr.append(str(None))
            else:
                arr.append(str(n.val))
            if n is not None:
                if n.left or n.right:
                    r.append(n.left)
                    r.append(n.right)

        return str(arr)

def build_tree(arr, cls=None):
    root = TreeNode(arr.pop(0))
    q = [root]
    while q:
        n = q.pop(0)
        if arr:
            if arr[0] is not None:
                l = TreeNode(arr.pop(0))
                n.left = l
                q.append(l)
            else:
                arr.pop(0)
        if arr: 
            if arr[0] is not None:
                r = TreeNode(arr.pop(0))
                n.right = r
                q.append(r)
            else:
                arr.pop(0)
    return root
