# python2
import collections
class Solution(object):
    def gardenNoAdj(self, N, paths):
        # Time Complexity O(E+V*k)
        # k is max number of edges for each V
        # Runtime: 652 ms, faster than 17.24%
        # Memory Usage: 19.6 MB, less than 68.10%
        if N <= 4:
            return range(1,N+1)
        if len(paths) == 0:
            return [1]*N

        graph = collections.defaultdict(list)
        for p in paths:
            x,y = p
            graph[x].append(y)
            graph[y].append(x)

        d = {}
        ret = [1]*N
        for g in graph.keys():
            if g == 2:
                pass
            flowers = {1:1, 2:1, 3: 1, 4: 1}
            if not d.has_key(g):
                for c in graph[g]:
                    if d.has_key(c) and flowers.has_key(d[c]):
                        del flowers[d[c]]
                d[g] = flowers.keys()[0]
                del flowers[d[g]]

        for i in d.keys():
            ret[i-1] = d[i]

        return ret

s = Solution()
print s.gardenNoAdj(5, [[3,4],[4,5],[3,2],[5,1],[1,3],[4,2]]) # [1,1,2,3,2]
print s.gardenNoAdj(3, [[1,2],[2,3],[3,1]]) # [1,2,3]
print s.gardenNoAdj(5, [[4,1],[4,2],[4,3],[2,5],[1,2],[1,5]]) # [1,2,1,3,3]

