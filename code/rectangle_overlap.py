# python 2
class Solution(object):
    def isRectangleOverlap(self, rec1, rec2):
        # If rectangles overlap their intersection is a rectangle with positive area
	# So its height and width should be positive
	# max(R1[x],R2[x]) < min(R1[y]R2[y]) for both co-ordinates
	# Runtime: 32 ms, faster than 9.31%
	# Memory Usage: 12.6 MB, less than 86.49%
	if max(rec1[0], rec2[0]) < min(rec1[2],rec2[2]) and \
            max(rec1[1], rec2[1]) < min(rec1[3],rec2[3]):
                return True
        return False

s = Solution()
print s.isRectangleOverlap([5,15,8,18], [0,3,7,9]) # False
print s.isRectangleOverlap([2,17,6,20], [3,8,6,20]) # True

