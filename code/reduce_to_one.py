# python 2
class Solution():
    def reduce_to(self, num):
	# recurse for each condition. if +1 or - 1 try both and use min
	# Time Complexity is O(log(n))
	if num == 1:
	    return 0
	if num % 2 == 0:
	    return 1 + self.reduce_to(num / 2)
	else:
	    g = self.reduce_to(num + 1)
	    s = self.reduce_to(num - 1)
	    return 1 + min(g, s)
	print num

s = Solution()
print s.reduce_to(15)


