# python2
class Solution():
    # Time Complexity n(log n)
    def topKFrequent(self, reviews, words, k):
        word_freq = {}
        for w in words:
            word_freq[w.lower()] = 0

        seen = {}
        for review in reviews:
            wds = review.split()
            for w in wds:
                # Clean word from punctuation
                alnum_word = ''
                for c in w:
                    if c.isalnum():
                        alnum_word += c.lower()

                if not seen.has_key(alnum_word):
                    seen[alnum_word] = 1
                    if word_freq.has_key(alnum_word):
                        word_freq[alnum_word] += 1
            seen = {}

        def sort_by(k):
            return (-word_freq[k], k)
        sorted_words = sorted(words, key=sort_by)
        #print word_freq
        #print sorted_words
        return sorted_words[:k]

s = Solution()
reviews = [
    "I love anacell Best services; Best services provided by anacell!",
    "betacellular has great services:",
    "deltacellular provides much better services than betacellular?",
    "cetracular is worse than anacell",
    "Betacellular is better than deltacellular.",

]
print s.topKFrequent(reviews, ["anacell", "betacellular", "cetracular", "deltacellular", "eurocell"], 2)
