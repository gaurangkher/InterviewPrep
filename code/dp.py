dp = {}

def solve(n):
    if n < 1:
        return 0;
    if n == 1:
        return 1
    if dp.has_key(n):
        return dp[n]

    ans = solve(n-1) + solve(n-3) + solve(n-5)
    dp[n] = ans
    print ans
    return ans


print solve(7)
print dp
