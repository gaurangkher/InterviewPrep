#python 2
class Solution(object):
    def minMeetingRooms(self, intervals):
        # Sort start and end times
        # Count start and end to get max occupance
        # Time Complexity O(nlong(n))
        # Time Complexity of Sort
        # Runtime: 64 ms, faster than 83.73% 
        # Memory Usage: 16.1 MB, less than 61.89%
        start = []
        end = []
        for i in intervals:
            start.append(i[0])
            end.append(i[1])

        start = sorted(start)
        end = sorted(end)

        i = 0
        j = 0
        occupied = 0
        max_rooms = 0
        while i < len(start):
            if start[i] < end[j]:
                occupied += 1
                if max_rooms < occupied:
                    max_rooms = occupied
                i += 1
            elif start[i] >= end[j]:
                occupied -= 1
                j += 1
                if start[i] == end[j]:
                    i += 1
        return max_rooms

s = Solution()
print s.minMeetingRooms([[0,30],[5,10],[15,20]]) # 2
print s.minMeetingRooms([[7,10],[2,4]]) # 1
