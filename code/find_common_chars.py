# python2
class Solution(object):
    def commonChars(self, A):
        # Hash
        # Time Complexity O(n)
        # Runtime: 52 ms, faster than 60.43%
        # Memory Usage: 13 MB, less than 11.52%
        l = len(A)
        d = []
        for s in A:
            ld = {}
            for c in s:
                if ld.has_key(c):
                    ld[c] += 1
                else:
                    ld[c] = 1
            d.append(ld)

        out = []
        c = d.pop()
        for k in c.keys():
            cnt = [c[k]]
            for d1 in d:
                if d1.has_key(k):
                    cnt.append(d1[k])
            if len(cnt) == l:
                occur = min(cnt)
                arr = [k]*occur
                out.extend(arr)

        return out

s = Solution()
print s.commonChars(["bella","label","roller"]) # ["e","l","l"]
