# python
class Solution(object):

    def valid(self, x, y, board, visits):
        if x >= 0 and x < len(board):
            if y >= 0 and y < len(board[x]):
                if (x,y) not in visits:
                    return True
        return False

    def dfs(self, position, word, visits, board):
        if not word:
            return True
        x = position[0]
        y = position[1]
        visits.append((x,y))
        c = word[0]
        found = False


        for p in ([[x+1, y], [x-1, y], [x, y+1], [x, y-1]]):
            test_v = []
            for q in visits:
                test_v.append((q[0], q[1]))
            if self.valid(p[0], p[1], board, test_v) and board[p[0]][p[1]] == c:
                found = self.dfs(p, word[1:], test_v, board)
                if found:

                    break

        return found

    def exist(self, board, word):
        # Time Complexity O(N * 4 ^L)
        # N = number of rows x columns of grid
        # L is number of characters in word
        # 4 because we could be matched in all directions 
        find_map = {}
        visits = []
        for i in range(len(board)):

            for j in range(len(board[i])):
                val = board[i][j]

                if find_map.has_key(val):
                    find_map[val].append([i,j])
                else:
                    find_map[val] = [[i,j]]


        found = False
        if not find_map.has_key(word[0]):
            return False
        starts = find_map[word[0]]
        for start in starts:

            visits = []
            found = self.dfs(start, word[1:], visits, board)
            if found:
                return found


        return found


s = Solution()
print s.exist([
    ["A","B","C","E"],
    ["S","F","C","S"],
    ["A","D","E","E"]
    ], "ABCCED"
)
