# python2
class Solution(object):
    def decodeString(self, s):
        # Tricky and tedious with lot of edge cases
        # Time Complexity O(n)
        # Runtime: 12 ms, faster than 96.41%
        # Memory Usage: 12.7 MB, less than 82.42%
        stack = []
        decoded = ''
        i = 0
        d = {}
        while i < len(s):
            c = s[i]
            if c.isalpha() and not stack:
                decoded += c
            elif c.isalpha() and stack:
                k = stack[-1]
                d[k]['val'] += c
            elif c.isdigit():
                num = ''
                while s[i].isdigit():
                    num += s[i]
                    i+= 1 #build number
                stack.append(i)
                d[i] = {'count': int(num), 'val': ''}
            elif c == ']':
                k = stack.pop()
                buf = ''
                for x in range(d[k]['count']):
                    buf += d[k]['val']
                if stack:
                    k = stack[-1]
                    d[k]['val'] += buf
                else:
                    decoded += buf

            i += 1

        return decoded

s = Solution()
print s.decodeString('3[a]2[bc]') # "aaabcbc"
print s.decodeString('3[a2[c]]') # "accaccacc"
print s.decodeString('2[abc]3[cd]ef') # "abcabccdcdcdef"
print s.decodeString('abc3[cd]xyz') # "abccdcdcdxyz"
print s.decodeString('10[lt]') # "ltltltltltltltltltlt"
print s.decodeString("3[z]2[2[y]pq4[2[jk]e1[f]]]ef") # "zzzyypqjkjkefjkjkefjkjkefjkjkefyypqjkjkefjkjkefjkjkefjkjkefef"


