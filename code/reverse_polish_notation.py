# python 2
class Solution(object):
    def evalRPN(self, tokens):
        # Time Complexity O(n)
        # Runtime: 52 ms, faster than 84.40% 
        # Memory Usage: 15.6 MB, less than 18.23%
        if not tokens:
            return 0
        if len(tokens) == 1:
            return tokens[0]
        stack = []
        out = 0
        for t in tokens:
            if t in ["+", "-", "*", "/"]:
                x = stack.pop()
                y = stack.pop()
                if t == "+":
                    out =  x + y
                elif t == "-":
                    out = y - x
                elif t == "*":
                    out = x*y
                else:
                    out = int(float(y)/x) # Python does a floor, which is a problem if x is negative
                stack.append(out)
            else:
                stack.append(int(t))

        return out

s = Solution()
print s.evalRPN(["2", "1", "+", "3", "*"]) # 9
print s.evalRPN(["10","6","9","3","+","-11","*","/","*","17","+","5","+"]) # 22


