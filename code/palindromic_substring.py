# python 2
class Solution(object):
    def search(self, s, i, j):
        count = 0
        while i >= 0  and j < len(s) and s[i] == s[j]:
            count += 1
            i -= 1
            j += 1
        return count

    def countSubstrings(self, s):
        # Assume center and expand out
        # Time Complexity O(N^2)
        # Runtime: 100 ms, faster than 79.59%
        # Memory Usage: 12.7 MB, less than 84.77%
        count = 0
        for i in range(len(s)-1):
            count += self.search(s,i,i)
            count += self.search(s,i,i+1)
        last =  len(s) - 1
        count += self.search(s, last, last)
        return count
