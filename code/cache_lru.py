# python2
class DLL():
    def __init__(self, k, val):
        self.key = k
        self.value = val
        self.prev = None
        self.next = None

    def __repr__(self):
        return '(%s, %s)' % (self.key, self.value)

class LRUCache(object):

    def __init__(self, capacity):
        # Implement with O(k) constant time, get and put methods
        # Use hash and doubly linked list
        # Runtime: 332 ms, faster than 40.09%
        # Memory Usage: 22.8 MB, less than 31.28%
        self.capacity = capacity
        self.cache = {}
        self.head = DLL('head', 'v')
        self.tail = DLL('tail', 'v')
        self.head.next = self.tail
        self.tail.prev = self.head

    def print_dl(self):
        n = self.head
        arr = []
        while n != self.tail:
            n = n.next
            arr.append((n.key, n.value))
        return arr

    def add_node(self, k ,v):
        #todo
        tmp = self.head.next
        d = DLL(k, v)
        self.cache[k] = d
        self.head.next = d
        d.prev = self.head
        d.next = tmp
        tmp.prev = d

    def remove_node(self, k):
        #todo
        node = self.cache[k]
        p = node.prev
        n = node.next
        p.next = n
        n.prev = p
        val = node.value
        del self.cache[k]
        return val

    def remove_tail(self):
        #todo
        tmp = self.tail.prev
        p = tmp.prev
        p.next = self.tail
        self.tail.prev = p
        del self.cache[tmp.key]

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        #print self.cache
        if self.cache.has_key(key):
            d = self.cache[key]
            value = self.remove_node(key)
            self.add_node(key, value)
            return d.value
        return -1

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: None
        """
        if not self.cache.has_key(key):
            if len(self.cache.keys()) >= self.capacity:
                self.remove_tail()
            self.add_node(key, value)
        else:
            self.remove_node(key)
            self.add_node(key, value)
        #print self.print_dl()
        return None

# Your LRUCache object will be instantiated and called as such:
obj = LRUCache(2)
output = [obj.put(2, 1), obj.put(1,1), obj.put(2,3), obj.put(4,1), obj.get(1), obj.get(2)]
expected = [None,None,None,None,-1,3]
print 'Match %s' % (output == expected)

# param_1 = obj.get(key)
# obj.put(key,value)
