# python2
# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def __repr__(self):
        s = '%s' % self.val
        return '%s -> %s' % (s, str(self.next))

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        # Time Complexity O(n)
        # Runtime: 56 ms, faster than 91.56% 
        # Memory Usage: 12.7 MB, less than 65.05%
        i = 0
        carry = 0
        tsum = 0
        ret = None
        node = None
        while l1 or l2:

            s = carry
            if l1:
                s += l1.val
                l1 = l1.next
            if l2:
                s += l2.val
                l2 = l2.next
            d = s
            if s >= 10:
                d = s - 10
                carry = 1
            else:
                carry = 0

            if node is None:
                node = ListNode(d)
                ret = node
            else:
                node.next = ListNode(d)
                node = node.next

            i += 1

        if carry:
            node.next = ListNode(carry)
        return ret

from lib.utils import build_linked_list
l1 = build_linked_list([2,4,3], cls=ListNode)
l2 = build_linked_list([5,6,4], cls=ListNode)
s = Solution()
print s.addTwoNumbers(l1, l2) # 7 -> 0 -> 8 -> None
