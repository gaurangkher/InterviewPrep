#python2
class Solution(object):
    def trap(self, height):
        # DP set max left height array and max right height array 
        # Time Complexity O(n)
        # Runtime: 36 ms, faster than 87.98% 
        # Memory Usage: 13.2 MB, less than 32.08%
        if not height:
            return 0
        l_arr = [height[0]]
        for i in range(1, len(height)):
            l_arr.append(max(l_arr[i-1], height[i]))

        r_arr = [0]*len(height)
        r_arr[-1] = height[-1]
        for i in range(len(height)-2, -1, -1):
            r_arr[i] = max(r_arr[i+1], height[i])

        count = 0
        for i in range(len(height)):
            count += (min(r_arr[i], l_arr[i]) - height[i])

        return count


    def trapPT(self, height):
        # TODO: two pointer
        pass


s = Solution()
print s.trap([0,1,0,2,1,0,1,3,2,1,2,1])
