#python2

class Solution():
    # Take available non-zero values and start writing to index.
    # If we encounter a zero, the index doesnt advance and we write the next non-zero value
    # Keep a count of zeros to be filled in later
    def moveZeroes(self, nums):
        idx=0
        for n in nums:
            if n!=0:
                nums[idx]=n
                idx+=1
        for i in range(idx,len(nums)):
            nums[i]=0
        return nums

s = Solution()
print s.moveZeroes([0,1,0,3,12])
print s.moveZeroes([1,0,1])
