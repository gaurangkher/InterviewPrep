# python 2
class Solution(object):
    def lengthOfLongestSubstring(self, s):
        # Time Complexity O(n)
        # Runtime: 48 ms, faster than 70.01%
        # Memory Usage: 13.6 MB, less than 10.45%
        start = 0
        d = {}
        max_len = 0
        for i,c in enumerate(s):
            if d.has_key(c):
                max_len = max(max_len, i-start)
                start = max(start,d[c] + 1)
                d[c] = i
            else:
                d[c] = i

        max_len = max(max_len, len(s)-start)
        return max_len

s = Solution()
print s.lengthOfLongestSubstring("abba") # 2
print s.lengthOfLongestSubstring("pwwkew") # 3
print s.lengthOfLongestSubstring("abcabcbb") # 3
print s.lengthOfLongestSubstring("") # 0
print s.lengthOfLongestSubstring("ababcdex") # 6
