# python2
class Solution(object):
    def maxSubArray(self, nums):
        # Kadane's DP algo
        # Time Complexity O(n)
        # Runtime: 88 ms, faster than 17.19%
        # Memory Usage: 13.2 MB, less than 64.64%
        if not nums:
            return None
        msum = nums.pop(0)
        csum = msum
        for i in nums:
            csum = max(i, csum + i)
            msum = max(msum, csum)
            #print 'csum %s msum %s' % (csum, msum)
        return msum


    def maxSubArrayBrute(self, nums):
        # Brute Force
        # Re-use Cumulative sum so
        # Time Complexity is O(n^2) and not O(n^3)
        # Leetcode TLE
        msum = nums[0]

        for i in range(len(nums)):
            csum = nums[i]
            if csum > msum:
                msum = csum
            for j in range(i+1, len(nums)):
                csum += nums[j]
                if csum > msum:
                    msum = csum

        return msum


class SolutionFull(object):
    def maxSubArray(self, nums):
        # Kadane's DP algo
        # Time Complexity O(n)
        # Runtime: 88 ms, faster than 17.19%
        # Memory Usage: 13.2 MB, less than 64.64%
        if not nums:
            return None
        msum = nums[0]
        csum = msum
        start = 0
        end = 0
        mstart = 0
        mend = 0
        for idx, i in enumerate(nums[1:]):
            csum += i
            end = idx + 1
            if csum < i:
                csum = i
                start = idx + 1
            if csum > msum:
                msum = csum
                mstart = start
                mend = end
            #print 'csum %s msum %s' % (csum, msum)
        return msum, mstart, mend


    def maxSubArrayBrute(self, nums):
        # Brute Force
        # Re-use Cumulative sum so
        # Time Complexity is O(n^2) and not O(n^3)
        # Leetcode TLE
        msum = nums[0]
        start = 0
        end = 0
        for i in range(len(nums)):
            csum = nums[i]
            if csum > msum:
                msum = csum
                start = i
                end = i
            for j in range(i+1, len(nums)):
                csum += nums[j]
                if csum > msum:
                    msum = csum
                    start = i
                    end = j

        return msum, start , end

s = Solution()
print s.maxSubArray([-2,1,-3,4,-1,2,1,-5,4]) # 6
print s.maxSubArray([-2,1,-3,4,-1,2,1,-5,4]) # 6

sf = SolutionFull()
print sf.maxSubArrayBrute([-2,1,-3,4,-1,2,1,-5,4]) # 6,3,6
print sf.maxSubArray([-2,1,-3,4,-1,2,1,-5,4]) # 6,3,6
print sf.maxSubArray([-2,2,5,-11,6]) # 7,1,2
print sf.maxSubArrayBrute([-2,2,5,-11,6]) # 7,1,2
