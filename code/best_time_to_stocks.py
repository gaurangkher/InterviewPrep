#python2
class Solution(object):
    def maxProfit(self, prices):
        # Time Complexity O(n)
        # Runtime: 48 ms, faster than 85.55% 
        # Memory Usage: 13.7 MB, less than 77.97%
        lmin = 0
        lmax = 0
        for i, p in enumerate(prices):
            if p < prices[lmin]:
                lmin = i
            elif p  - prices[lmin] > lmax:
                lmax = p - prices[lmin]
        return lmax

s = Solution()
print s.maxProfit([7,1,5,3,6,4]) # 5
print s.maxProfit([7,6,4,3,1]) # 0
