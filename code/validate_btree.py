# python2
def construct_btree(arr):
    if not arr:
        return Node()
    r = arr.pop(0)
    root = Node(r)
    level = []
    if arr:
        l = Node(arr.pop(0))
        level.append(l)
        root.left = l
        r = Node(arr.pop(0))
        level.append(r)
        root.right = r
    new_level = []
    while arr:
        for e in level:
            if arr:
                l = Node(arr.pop(0))
                new_level.append(l)
                e.left = l
                r = Node(arr.pop[0])
                new_level.append(r)
                e.right = r
        level = new_level
    return root

class Node():
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
    def __repr__(self):
        arr = [self.val]
        q = [self.left, self.right]
        while q:
            n = q.pop(0)
            arr.append[n.val]
            q.append(n.left, n.right)

class Solution(object):
    # Recursion
    # Time Complexity: O(n)
    # Runtime: 60 ms, faster than 8.11% 
    # Memory Usage: 17.4 MB, less than 65.45%
    def traverse(self, node, left_max, right_min, valid):
        if not valid:
            return False
        if not node:
            return True

        if node.val >= left_max or node.val <= right_min:
            return False

        val =  self.traverse(node.left, min(node.val, left_max), right_min, valid) 
        if not val:
            return False
        val =  self.traverse(node.right, left_max, max(node.val, right_min), valid)
        if not val:
            return False
        return True


    def isValidBST(self, root):
        # Time Complexity is O(N)
        # Runtime: 24 ms, faster than 99.32%
        # Memory Usage: 17.5 MB, less than 45.44%
        # if is left and left.val < node.val
        if root is None:
            return True
        return self.traverse(root, float('inf'), float('-inf'), True)


tree_arr = [2,1,3]
print construct_btree(tree_arr)
