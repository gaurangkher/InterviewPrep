class UnionFind():
    def __init__(self, grid):
        self.sets = {}
        self.count = 0
        for row in range(len(grid)):
            for column in range(len(grid[row])):
                if grid[row][column] == 1 or grid[row][column] == '1':
                    self.sets[(row, column)] = {'rank': 0, 'parent': (row,column)}
                    self.count +=1

    def find(self, x):
        if self.sets[x]['parent'] != x:
            self.sets[x]['parent'] = self.find(self.sets[x]['parent'])
        return self.sets[x]['parent']

    def union(self, x, y):
        x_root = self.find(x)
        y_root = self.find(y)
        if x_root == y_root:
            return
        if self.sets[x_root]['rank'] != self.sets[y_root]['rank']:
            self.sets[x_root]['parent'] = y_root
        else:
            self.sets[y_root]['parent'] = x_root
        self.count -= 1
        return

class Solution():
    def is_land(self, r, c, grid):
        if r < 0 or r >= len(grid):
            return False
        if c < 0 or c >= len(grid[r]):
            return False
        if grid[r][c] == 1 or grid[r][c] == '1':
            return True
        return False

    def numIslands(self, grid):
        uf = UnionFind(grid)
        for row in range(len(grid)):
            for column in range(len(grid[row])):
                # For all land peices that are their own sets
                cell = str(grid[row][column])
                if cell == '1' and uf.sets.has_key((row, column)):
                    for direction in ([[row+1,column], [row-1,column], [row, column-1], [row, column+1]]):
                        if self.is_land(direction[0], direction[1], grid):
                            uf.union((row, column), (direction[0], direction[1]))
                            # Mark  visited
                            grid[row][column] = 2
        return uf.count

s = Solution()
grid = [
    [1,1,1,1,0],
    [1,1,0,1,0],
    [1,1,0,0,0],
    [0,0,0,0,0]
]
print s.numIslands(grid)
grid = [
    [1,1,0,0,0],
    [1,1,0,0,0],
    [0,0,1,0,0],
    [0,0,0,1,1]
]
print s.numIslands(grid)
grid = [
    ['1','1','1','1','1','1','1'],
    ['0','0','0','0','0','0','1'],
    ['1','1','1','1','1','0','1'],
    ['1','0','0','0','1','0','1'],
    ['1','0','1','0','1','0','1'],
    ['1','0','1','1','1','0','1'],
    ['1','1','1','1','1','1','1']]

print s.numIslands(grid)
