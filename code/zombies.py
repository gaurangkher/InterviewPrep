# python2

class Solution():
    # BFS
    # Time Complexity is O(n)
    def __init__(self):
        self.graph = []
        self.zombies = []
        self.people_count = 0

    def is_valid(self, r, c):
        if r < 0 or r >= len(self.graph):
            return False
        if c < 0 or c >= len(self.graph[r]):
            return False
        if self.graph[r][c] == 1:
            return False
        return True

    def infect(self, z):
        r = z[0]
        c = z[1]
        new_zombies = []
        for cell in [(r+1,c), (r-1,c), (r, c+1), (r, c-1)]:
            if self.is_valid(cell[0], cell[1]):
                self.graph[cell[0]][cell[1]] = 1
                new_zombies.append(cell)
                self.people_count -= 1
        return new_zombies

    def hourstozombies(self, graph):
        self.graph = graph
        hour = 0
        zombies = []
        for row in xrange(len(graph)):
            for col in xrange(len(graph[row])):
                self.people_count += 1
                if graph[row][col] == 1:
                    self.people_count -= 1
                    zombies.append((row,col))

        while self.people_count > 0:
            hour += 1
            new_zombies = []
            for z in zombies:
                new_zombies.extend(self.infect(z))
            zombies = new_zombies

        return hour


s = Solution()
graph = [
    [0, 1, 1, 0, 1],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0]
]
print s.hourstozombies(graph)
