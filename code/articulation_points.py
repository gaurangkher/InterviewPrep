# python2

class UnionFind():
    def __init__(self, nodes, exclude):
        self.set = {}
        self.d_sets = 0
        for n in nodes:
            if n != exclude:
                self.set[n] = {'rank': 0, 'parent': n}
                self.d_sets +=1

    def find(self, x):
        if self.set[x]['parent'] != x:
            self.set[x]['parent'] = self.find(self.set[x]['parent'])
        return self.set[x]['parent']

    def union(self, x, y):
        x_root = self.find(x)
        y_root = self.find(y)

        if x_root == y_root:
            return
        if self.set[x_root]['rank'] != self.set[y_root]['rank']:
            self.set[x_root]['parent'] = y_root
        else:
            self.set[y_root]['parent'] = x_root
        self.d_sets -= 1

    def is_complete(self):
        if self.d_sets > 1:
            return False
        return True

class Solution():
    # Brute Force
    def getArticulationPoints(self, numNodes, edges):
        # For each node, remove node and see if we get rest of nodes
        # Assume nodes are 0 to numNodes-1
        articulation_points = []
        for an in range(numNodes):
            n = an + 1
            g = UnionFind(range(1, numNodes+1), n)
            for e in edges:
                if n not in e:
                    g.union(e[0], e[1])
            if not g.is_complete():
                articulation_points.append(n)

        return articulation_points


s = Solution()
# 3 -- 4 -- 2
#  \  / 
#    1 -- 2
print s.getArticulationPoints(5, [[1, 2], [1, 3], [3, 4], [1, 4], [4, 5]]) # [1, 4]

# 1    4
# | \ /  \
# |  2    6
# | / \  /
# 3    5
print s.getArticulationPoints(6, [[1, 2], [1, 3], [2, 3], [2, 4], [2, 5], [4, 6], [5, 6]]) # [2]

#    1        7
#   / \      / \
#  2 - 3 -- 6   8
#      |     \ /
#      4-- 5  9

print s.getArticulationPoints(9, [[1, 2], [1, 3], [2, 3], [3, 4], [3, 6], [4, 5], [6, 7], [6, 9], [7, 8], [8, 9]])
# [3,4,6]
