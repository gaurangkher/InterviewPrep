# python 2

class Solution(object):
    def __init__(self):
        self.cache = {}
    def check(self, s, wordDict):
        if self.cache.has_key(s):
            return self.cache[s]
        if not s:
            return True
        w = ''
        found = False
        for c in s:
            w += c
            if w in wordDict:
                found = found or self.check(s[len(w):], wordDict)
        if not found:
            self.cache[s] = False
            return self.cache[s]
        self.cache[s] = True
        return self.cache[s]

    def wordBreak(self, s, wordDict):
        # Recursion with memoization
        # Without memoization TLE
        # Time Complexity O(n^2)
        # Runtime: 36 ms, faster than 42.77% 
        # Memory Usage: 12.9 MB, less than 42.48% 
        return self.check(s, wordDict)


s = Solution()
print s.wordBreak("leetcode", ["leet", "code"]) # True
print s.wordBreak("applepenapple", ["apple", "pen"]) # True
print s.wordBreak("catsandog", ["cats", "dog", "sand", "and", "cat"]) # False

