# python 2

class Solution(object):
    def mostCommonWord(self, paragraph, banned):
        # Runtime: 56 ms, faster than 5.74%
        # Memory Usage: 12.8 MB, less than 46.77%
        banned_lc = []
        for b in banned:
            banned_lc.append(b.lower())
        counts = {}
        max_k = None
        max_val = 0
        for line in paragraph.split('\n'):
            for word in line.split(' '):
                for x in ['!', '.', '?', ';',',', "'"]:
                    word = word.replace(x, ' ')
                for wn in word.split(' '):
                    w = wn.lower()
                    if w in banned_lc or not w:
                        continue
                    if counts.has_key(w):
                        counts[w] += 1
                    else:
                        counts[w] = 1
                    if counts[w] > max_val:
                        max_k = w
                        max_val = counts[w]

        #print counts
        return max_k

s = Solution()
print s.mostCommonWord("Bob hit a ball, the hit BALL flew far after it was hit.", ["hit"])
