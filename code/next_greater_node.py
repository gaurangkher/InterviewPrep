# python2
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def __str__(self):
        ret = '%s' % self.val
        if self.next:
            ret = ret + ' -> %s' % str(self.next)
        return ret

class Solution():
    def nextLargerNodes(self, head):
        # Time Complexity O(n)
        # Runtime: 320 ms, faster than 99.40%
        # Memory Usage: 21.7 MB, less than 42.85%

        stack = []
        result = []
        count = 0
        while head is not None:
            result.append(0)
            while stack and stack[-1][0] < head.val:
                val, index = stack.pop()
                result[index] = head.val
            stack.append([head.val, count])
            count += 1
            head = head.next
        return result


def build_ll(arr):
    root = ListNode(arr.pop(0))
    n = root
    for e in arr:
        n.next = ListNode(e)
        n = n.next
    return root


from lib.utils import build_linked_list

ll = build_linked_list([2,1,5], cls=ListNode)
s = Solution()
print s.nextLargerNodes(ll)


ll = build_linked_list([1,7,5,1,9,2,5,1], cls=ListNode)
print s.nextLargerNodes(ll)

