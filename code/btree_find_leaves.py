# python 2
class Solution(object):
    def __init__(self):
        self.d = {}
    def dfs(self, node):
        if not node.left and not node.right:
            if self.d.has_key(0):
                self.d[0].append(node.val)
            else:
                self.d[0] = [node.val]
            return 0
        l = 0
        if node.left:
            l = self.dfs(node.left)
        if node.right:
            r = self.dfs(node.right)
            l = max(l,r)

        v = 1 + l
        if self.d.has_key(v):
            self.d[v].append(node.val)
        else:
            self.d[v] = [node.val]
        return v

    def findLeaves(self, root):
        # Time Complexity N + klog(k) where k = logN
        # Runtime: 16 ms, faster than 84.90%
        # Memory Usage: 13.5 MB, less than 15.51%
        if not root:
            return []

        self.dfs(root)
        ret = []
        for k in sorted(self.d.keys()):
            ret.append(self.d[k])

        return ret

from lib.utils import build_tree
s = Solution()
print s.findLeaves(build_tree([1,2,3,4,5]))
