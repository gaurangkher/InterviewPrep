class Solution(object):
    def partitionLabels(self, S):
        # Runtime: 88 ms, faster than 5.52%
        # Memory Usage: 12.7 MB, less than 73.56%
        # Time Complexity O(n^2^)
        # We keep a hash of chars seen and at what index are they stored
        # If we havent not seen the char we create a new string
        # If we have seen it, combine all string from the char index to last and
        # mark all chars in each string with new index
        atmost_one = []
        char_hash = {}
        for char in S:
            if char in char_hash:
                # Combine from char[hash] to end of list
                index = char_hash[char]
                combine = ''
                for i in range(len(atmost_one), index, -1):
                    el = atmost_one.pop()
                    for c in el:
                        char_hash[c] = index
                    combine = el + combine
                atmost_one.append(combine + char)
            else:
                atmost_one.append(char)
                char_hash[char] = len(atmost_one) - 1
            print 'atmost %s hash %s' % (str(atmost_one), str(char_hash))
        ls = []
        for x in atmost_one:
            ls.append(len(x))

        return ls

class PointersSolution(object):
    # Store the max location of chars
    # Use start and end pointer to get partitions
    # end pointer would pick the max value of seen chars
    # as we traverse if we reach end pointer, we say we detect a partition end
    # Time Complexity : O(n)
    # Runtime: 16 ms, faster than 99.28%
    # Memory Usage: 12.9 MB, less than 7.54%
    def partitionLabels(self, S):
        max_location = {}
        for index, char in enumerate(S):
            max_location[char] = index
        left = 0
        right = 0
        ls = []
        print max_location
        for index, c in enumerate(S):
            right =  max(right, max_location[c])
            if right == index:
                ls.append(right - left + 1)
                left = index + 1
        return ls

s = Solution()
#print s.partitionLabels('ababcbacadefegdehijhklij')

s = PointersSolution()
print s.partitionLabels('ababcbacadefegdehijhklij')
