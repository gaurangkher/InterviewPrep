# python2
class Solution(object):
    def detectCapitalUse(self, word):
        # Time Complexity O(n)
        # Runtime: 8 ms, faster than 99.86%
        # Memory Usage: 12.8 MB, less than 35.33% 
        is_correct = True
        if not word:
            return False
        any_upper = False
        any_lower = False
        is_first = word[0].isupper()
        for i in range(1, len(word)):
            c = word[i]
            if c.islower():
                if any_upper:
                    return False
                any_lower = True
            elif c.isupper():
                if not is_first or any_lower:
                    return False
                any_upper = True

        return is_correct

s = Solution()
print s.detectCapitalUse("USA") # True
print s.detectCapitalUse("FlaG") # False
print s.detectCapitalUse("great") # True
