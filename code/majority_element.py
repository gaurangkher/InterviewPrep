# python2
class Solution(object):
    def majorityElement(self, nums):
        # Hash
        # Time Complexity O(n)
        # Runtime: 192 ms, faster than 37.90%
        # Memory Usage: 14 MB, less than 95.38%
        d = {}
        max_num = None
        max_count = 0
        for n in nums:
            d.setdefault(n, 0)
            d[n] += 1
            if d[n] > max_count:
                max_num = n
                max_count = d[n]
        return max_num

s = Solution()
print s.majorityElement([3,2,3]) # 3

print s.majorityElement([2,2,1,1,1,2,2]) # 2
