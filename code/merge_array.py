# python2
class Solution():
    def merge(self, nums1, m, nums2, n):
        x = 0
        for i in range(n):
            while nums1[x] and nums2[i] > nums1[x]:
                x += 1
            if not nums1[x]:
                print 'here'
                nums1[x] = nums2[i]
            else:
                for y in range(i+m, x, -1):
                    nums1[y] = nums1[y-1]
                nums1[x] = nums2[i]

        return

s = Solution()
nums1 = [1,2,3,0,0,0]
s.merge(nums1, 3, [2,5,6], 3)
print nums1

# Fails for 
nums1 = [-1,0,0,3,3,3,0,0,0]
s.merge(nums1, 6, [1,2,2], 3)
print num1 # Expected [-1,0,0,1,2,2,3,3,3]

