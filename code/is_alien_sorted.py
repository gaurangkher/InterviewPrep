# python 2
class Solution(object):
    def isAlienSorted(self, words, order):
        # Time Complexity O(n)
	# Runtime: 48 ms, faster than 7.73%
	# Memory Usage: 12.7 MB, less than 68.40%
	if len(words) < 2:
            return True
        weights = {}
        count = 0
        for c in order:
            weights[c] = count
            count += 1

        for i in range(len(words) - 1):
            first = words[i]
            second = words[i+1]

            equal = False
            for i in range(min(len(first), len(second))):

                if weights[first[i]] < weights[second[i]]:
                    equal = False
                    break
                if weights[first[i]] > weights[second[i]]:
                    return False
                else:
                    equal = True

            if equal and len(second) < len(first):
                return False

        return True

s = Solution()
print s.isAlienSorted(["hello","leetcode"], "hlabcdefgijkmnopqrstuvwxyz") # True

