# python 2
class Solution(object):
    def areSentencesSimilar(self, sentence1, sentence2, similarPairs):
        # Time Complexity O(N+P)
        # We check all words in sentence against all matches for a word
        #Runtime: 32 ms, faster than 85.44%
        # Memory Usage: 12.8 MB, less than 43.69% 
        if len(sentence1) != len(sentence2):
            return False
        d = {}
        for p in similarPairs:
            if d.has_key(p[0]):
                d[p[0]].append(p[1])
            else:
                d[p[0]] = [p[1]]

        for i in range(len(sentence1)):
            w1 = sentence1[i]
            w2 = sentence2[i]
            equal = False
            if d.has_key(w1):
                if w2 in d[w1]:
                    equal = True

            if not equal and d.has_key(w2):
                if w1 in d[w2]:
                    equal = True
            if w1 == w2:
                equal = True
            if not equal:
                return False
        return True

s = Solution()
print s.areSentencesSimilar(
    ["great","acting","skills"],
    ["fine","drama","talent"], 
    [["great","fine"],["drama","acting"],["skills","talent"]]
) # True

