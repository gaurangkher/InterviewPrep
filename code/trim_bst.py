# python2
class Solution(object):
    def trimBST(self, root, low, high):
        # Time Complexity O(n)
        # Runtime: 48 ms, faster than 49.36%
        # Memory Usage: 21.3 MB, less than 5.40%
        if not root:
            return
        if root.val < low:
            return self.trimBST(root.right, low, high)
        if root.val > high:
            return self.trimBST(root.left, low, high)

        if root.left:
            root.left = self.trimBST(root.left, low, high)
        if root.right:
            root.right = self.trimBST(root.right, low, high)
        return root

from lib.utils import build_tree
s = Solution()
print s.trimBST(build_tree([1,0,2]), 1,2) # [1, None, 2]
print s.trimBST(build_tree([3,0,4,None,2,None,None,1]), 1,3) # ['3', '2', 'None', '1', 'None'] 
