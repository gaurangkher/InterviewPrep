from threading import Event, Thread
class FooBar(object):
    def __init__(self, n):
        # Runtime: 44 ms, faster than 62.69%
        # Memory Usage: 12.8 MB, less than 86.57%
        self.n = n
        self.e = Event()
        self.b = Event()
        self.b.set()

    def foo(self, printFoo):
        for i in xrange(self.n):
            # printFoo() outputs "foo". Do not change or remove this line.
            self.b.wait()
            self.b.clear()
            printFoo()
            self.e.set()

    def bar(self, printBar):
        for i in xrange(self.n):
            self.e.wait()
            self.e.clear()
            # printBar() outputs "bar". Do not change or remove this line.
            printBar()
            self.b.set()

def printBar():
    print 'bar'
def printFoo():
    print 'foo'

fb = FooBar(5)
t1 = Thread(target=fb.foo, args=(printFoo,))
t2 = Thread(target=fb.bar, args=(printBar,))
t1.start()
t2.start()

