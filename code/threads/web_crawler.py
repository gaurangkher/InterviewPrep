# python2
import threading
import time
# This is HtmlParser's API interface.
# You should not implement it, or speculate about its implementation
class HtmlParser(object):
    def __init__(self, d):
        self.d = d
    def getUrls(self, url):
        time.sleep(4)
        if self.d.has_key(url):
            return self.d[url]
        return []

class Solution(object):
    def __init__(self):
        self.q = []
        self.visited= {}
        self.host = ''
        self.lock = threading.Lock()

    def process(self, urls):
        with self.lock:
            for url in urls:
                parts = url.split('/')
                if parts[2] == self.host:
                    if not self.visited.has_key(url):
                        self.q.append(url)
                        self.visited[url] = 1

    def worker(self, url, parser):
        urls = []
        for u in parser.getUrls(url):
            urls.append(u)
        self.process(urls)

    def crawl(self, startUrl, htmlParser):
        # Runtime: 1396 ms, faster than 5.95%
        # Memory Usage: 20.8 MB, less than 45.24%
        self.visited = {startUrl: 1}
        self.q = [startUrl]
        parts = startUrl.split('/')
        self.host = parts[2]
        workers = []
        while self.q or workers:
            if self.q:
                url = self.q.pop(0)
                t = threading.Thread(target=self.worker, args=(url, htmlParser))
                workers.append(t)
                t.start()
            if not self.q:
                result = False
                # Wait until atleast one returns
                while not result:
                    for i in range(len(workers)):
                        w = workers[i]
                        if not w.is_alive():
                            del workers[i]
                            result = True
                            break
                    if result or not workers:
                        result = True

        #print self.visited
        return self.visited.keys()

d = {
    'http://news.yahoo.com/news/topics/islamic-state': [
        'http://news.yahoo.com/oil-price-fundamental-weekly-forecast-200421659.html',
        'http://news.yahoo.com/latest-israeli-troops-kill-gunman-075217877.html',
    ],
    'http://news.yahoo.com/oil-price-fundamental-weekly-forecast-200421659.html': [
        'http://news.yahoo.com/elizabeth-warren-unveils-plan-reduce-193138508.html',
        'http://news.yahoo.com/news/topics/islamic-state',
        'http://news.yahoo.com/us-stocks-wall-street-set-125725929.html',
        'http://news.yahoo.com/news/topics/pets',
        "http://news.yahoo.com/liam-hemsworth-doesnt-want-talk-161900831.html",
    ],
    'http://news.yahoo.com/news/topics/pets': [
        'http://news.yahoo.com/m/566613eb-406d-3f03-8007-c764674d10ea/after-facebook-and-twitter%2C.html',
        "http://news.yahoo.com/news/topics/islamic-state",
    ],
    'http://news.yahoo.com/m/566613eb-406d-3f03-8007-c764674d10ea/after-facebook-and-twitter%2C.html': [
        "http://news.yahoo.com/middle-school-teacher-faces-backlash-after-handing-out-genderidentity-worksheet-233129871.html",
        "http://news.yahoo.com/miss-usa-almost-didnt-wear-her-hair-natural-after-overhearing-criticism-090000447.html",
        "http://news.yahoo.com/middle-school-teacher-faces-backlash-after-handing-out-genderidentity-worksheet-233129871.html",

    ],
    "http://news.yahoo.com/liam-hemsworth-doesnt-want-talk-161900831.html": [
        "http://news.google.com/stories/articles/CAIiEKs.html"
    ]
}
parser = HtmlParser(d)
s = Solution()
print s.crawl("http://news.yahoo.com/news/topics/islamic-state", parser)
