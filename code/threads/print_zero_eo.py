from threading import Event, Thread
class ZeroEvenOdd(object):
    def __init__(self, n):
        # Runtime: 48 ms, faster than 50.00% 
        # Memory Usage: 13.8 MB, less than 55.88%
        self.n = n
        self.i = 0
        self.ze = Event()
        self.ee = Event()
        self.oe = Event()
        self.ze.set()

	# printNumber(x) outputs "x", where x is an integer.
    def zero(self, printNumber):
        while self.i < self.n:
            self.i += 1
            self.ze.wait()
            self.ze.clear()
            printNumber(0)
            if self.i % 2 == 0:
                self.ee.set()
            else:
                self.oe.set()

    def even(self, printNumber):
        i = 2
        while i <= self.n:
            self.ee.wait()
            self.ee.clear()
            printNumber(i)
            self.ze.set()
            i += 2

    def odd(self, printNumber):
        i = 1
        while i <= self.n:
            self.oe.wait()
            self.oe.clear()
            printNumber(i)
            self.ze.set()
            i += 2

zoe = ZeroEvenOdd(4)
def printNumber(i):
    print i
t1 = Thread(target=zoe.zero, args=(printNumber,))
t2 = Thread(target=zoe.even, args=(printNumber,))
t3 = Thread(target=zoe.odd, args=(printNumber,))
t1.start()
t2.start()
t3.start()

