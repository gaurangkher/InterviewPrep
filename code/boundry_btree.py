# python2
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution(object):
    def get_left(self, node, depth, arr):
        if depth == len(arr):
            if node.left or node.right:
                arr.append(node.val)

        if node.left:
            arr = self.get_left(node.left, depth+1, arr)
        elif node.right and depth:
            arr = self.get_left(node.right, depth+1, arr)

        return arr

    def get_right(self, node, depth, arr):
        if depth == len(arr):
            if node.left or node.right:
                arr.append(node.val)

        if node.right:
            arr = self.get_right(node.right, depth+1, arr)
        elif node.left and depth:
            arr = self.get_right(node.left, depth+1, arr)
        return arr

    def get_children(self, node,  arr):
        if node and node.left is None and node.right is None:
            arr.append(node.val)
        if node.left:
            arr = self.get_children(node.left,  arr)
        if node.right:
            arr = self.get_children(node.right,   arr)
        return arr

    def boundaryOfBinaryTree(self, root):
        # DFS, left, right and children
        # Time Complexity O(n)
        # Runtime: 28 ms, faster than 97.67%
        # Memory Usage: 15.6 MB, less than 86.05% 
        if not root:
            return None
        boundry = []
        left = []
        left =  self.get_left(root, 0, [])
        boundry = left

        children = []
        children = self.get_children(root, [])

        boundry = boundry + children
        right = []
        right = self.get_right(root, 0, [])
        right.reverse()
        if right:
            right.pop()

        boundry = boundry + right

        return boundry


from lib.utils import build_tree, TreeNode
t = build_tree([1,None,2,3,4])
s = Solution()
print s.boundaryOfBinaryTree(t) # [1,3,4,2]
t = build_tree([1,2,3,4,5,6,None,None, None, 7,8, 9,10])
print s.boundaryOfBinaryTree(t) # [1, 2, 4, 7, 8, 9, 10, 6, 3]
