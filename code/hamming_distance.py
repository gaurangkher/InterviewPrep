# python 2
class Solution(object):
    def hammingDistance(self, x, y):
        # Time Complexity O(1)
        # Runtime: 16 ms, faster than 80.96%
        # Memory Usage: 13.5 MB, less than 5.12%
        z = x^y
        d = 0
        while z:
            d += z&1
            z /= 2

        return d

s = Solution()
print s.hammingDistance(1,4) # 2

