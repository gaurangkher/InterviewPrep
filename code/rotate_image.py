#python2
class Solution(object):
    def rotate(self, matrix):
        # Transpose and the reverse
        # Time Complexity O(n^2)
        # Runtime: 24 ms, faster than 74.17%
        # Memory Usage: 12.8 MB, less than 37.25% 
        for i in range(len(matrix)):
            for j in range(i, len(matrix[i])):
                matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]

        for i in range(len(matrix)):
            matrix[i].reverse()

        return matrix

s = Solution()
print s.rotate([[1,2,3],[4,5,6],[7,8,9]]) # [[7,4,1],[8,5,2],[9,6,3]]


