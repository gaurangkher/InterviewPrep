# python
class Node():
    def __init__(self, name):
        self.name = name
        self.apple = False
        self.children = []

class Solution(object):
    def minTime(self, n, edges, hasApple):
        """
        :type n: int
        :type edges: List[List[int]]
        :type hasApple: List[bool]
        :rtype: int
        """
        nodes = {}
        for index in range(len(edges)):
            src, dest = edges[index]
            if not nodes.has_key(src):
                nodes[src] = Node(src)
                nodes[src].apple = hasApple[src]
            
            if not nodes.has_key(dest):
                nodes[dest] = Node(dest)
                nodes[dest].apple = hasApple[dest]
            nodes[src].children.append(nodes[dest])
            
        if nodes:
            count = self.dfs(nodes[0], 0)
        if count:
            count = count - 2
        
        return count
            
    def dfs(self, node, count):
        if node:
            for c in node.children:
                c = self.dfs(c, 0)
                if c:
                    node.apple = True
                count = count + c
          
            if node.apple:
                count = count + 2
            print 'Node %s Count %s' % (node.name, count)
            return count


s = Solution()
s.minTime(7, [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], [false,false,true,false,false,true,false])
