# python2

class Node():
    def __init__(self, x, y, depth):
        self.x = x
        self.y = y
        self.depth = depth
        self.prev = None

class Solution():
    # Time Complexity O(N)
    # BFS iterative solution
    def valid_route(self, node, tmap):
        if node.x >= 0 and node.x < len(tmap[0]) and node.y >= 0 and node.y < len(tmap):
            if tmap[node.x][node.y] != 'D' or tmap[node.x][node.y] != 'V':
                return True
        return False

    def findTreasure(self, tmap):
        steps = -1
        if not tmap:
            return steps

        nodes = [Node(0,0,0)]
        while nodes:
            #print nodes
            node = nodes.pop(0)
            if tmap[node.x][node.y] == 'X':
                if steps == -1:
                    steps = node.depth
                else:
                    steps = min(steps, node.depth )
            if tmap[node.x][node.y] == 'O':
                new_depth = node.depth + 1
                for x,y in [(node.x + 1, node.y), (node.x -1,node.y), (node.x, node.y +1), (node.x, node.y - 1)]:
                    n = Node(x, y, new_depth)
                    if self.valid_route(n, tmap):
                        nodes.append(n)

            tmap[node.x][node.y] = 'V'
        return steps



s = Solution()

m = [['O', 'O', 'O', 'O'],
     ['D', 'O', 'D', 'O'],
     ['O', 'O', 'O', 'O'],
     ['X', 'D', 'D', 'O']]
print s.findTreasure(m)
