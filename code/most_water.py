# python2
class Solution(object):
    def maxArea(self, height):
        # Two Pointer
        # Start at ends and bring pointers forward from smaller heights
        # Time Complexity O(n)
        # Runtime: 100 ms, faster than 93.37%
        # Memory Usage: 14.1 MB, less than 22.72% 
        start = 0
        end = len(height) - 1
        max_area = 0
        while start < end:
            area = (end - start) * min(height[start], height[end])
            if area > max_area:
                max_area = area
            if height[start] < height[end]:
                start += 1
            else:
                end -= 1

        return max_area


s = Solution()
print s.maxArea([1,8,6,2,5,4,8,3,7]) # 49

