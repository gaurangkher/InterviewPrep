# python 2
class Solution(object):
    def findUnsortedSubarray(self, nums):
        # Time Complexity O(n) ?
        # Runtime: 236 ms, faster than 43.38%
        # Memory Usage: 13.4 MB, less than 89.85%
        if len(nums) < 2:
            return 0
        stack = []
        start = len(nums) - 1
        end = 0
        for i in range(len(nums)):
            while stack and nums[stack[-1]] > nums[i]:
                start = min(start, stack.pop())
            stack.append(i)

        if start == len(nums) - 1:
            return 0
        stack = []
        for i in range(len(nums)-1,-1,-1):
            while stack and nums[stack[-1]] < nums[i]:
                end = max(end, stack.pop())
            stack.append(i)

        if start == end:
            return 0
        return end - start + 1

    def findUnsortedSubarray_sort(self, nums):
        # Time Complexity O(nlog(n))
        # Runtime: 236 ms, faster than 43.38%
        # Memory Usage: 13.4 MB, less than 89.85% 
        sorted_nums = sorted(nums)
        start = len(nums) - 1
        end = 0
        for i in range(len(nums)):
            if nums[i] != sorted_nums[i]:
                start = min(start, i)
                end = max(end, i)

        if end == 0:
            return 0
        return end - start + 1

s = Solution()
print s.findUnsortedSubarray([2, 6, 4, 8, 10, 9, 15]) # 5
print s.findUnsortedSubarray([1,2,3,3,3]) # 0
print s.findUnsortedSubarray([1]) # 0
print s.findUnsortedSubarray([1,3,5,4,2]) # 2

print s.findUnsortedSubarray_sort([2, 6, 4, 8, 10, 9, 15]) # 5
print s.findUnsortedSubarray_sort([1,2,3,3,3]) # 0
print s.findUnsortedSubarray_sort([1]) # 0
print s.findUnsortedSubarray_sort([1,3,5,4,2]) # 2
