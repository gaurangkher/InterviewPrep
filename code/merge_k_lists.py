# python 2 
class ListNode():
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def __str__(self):
        ret = '%s' % self.val
        if self.next:
            ret = ret + ' -> %s' % str(self.next)
        return ret

class Solution(object):
    def mergeTwoLists(self, l1, l2):
        # With this merge 2 lists performance is bad, code is ugly
        # Runtime: 296 ms, faster than 11.02%
        # Memory Usage: 24.6 MB, less than 5.04%
        if not l1 and not l2:
            return
        if not l1:
            return l2
        if not l2:
            return l1
        r = ListNode()
        if l1.val < l2.val:
            r.val = l1.val
            l1 = l1.next
        else:
            r.val = l2.val
            l2 = l2.next
        root = r

        while l1 is not None and l2 is not None:
            if l1.val < l2.val:
                r.next = ListNode(l1.val)
                l1 = l1.next
            else:
                r.next = ListNode(l2.val)
                l2 = l2.next
            r = r.next

        while l1:
            r.next = ListNode(l1.val)
            r = r.next
            l1 = l1.next
        while l2:
            r.next = ListNode(l2.val)
            l2 = l2.next
            r = r.next
        return root

    def mergeKLists(self, lists):
        # Divide and Conquer
        # Gives us Time complexity of nlog(k)
        # 
        if not lists:
            return
        if len(lists) == 1:
            return lists[0]
        mid = len(lists) // 2
        l1 = self.mergeKLists(lists[:mid])
        l2 = self.mergeKLists(lists[mid:])
        
        return self.mergeTwoLists(l1, l2)


from lib.utils import build_linked_list

lists = [
    build_linked_list([1,4,5], cls=ListNode),
    build_linked_list([1,3,4], cls=ListNode),
    build_linked_list([2,6], cls=ListNode)
]
s = Solution()
print s.mergeKLists(lists)
