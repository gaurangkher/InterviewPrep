# python 2
class Solution(object):
    def is_valid(self, pt, rooms):
        x,y = pt
        if x >= 0 and x < len(rooms):
            if y >= 0 and y < len(rooms[x]):
                if rooms[x][y] == 2147483647:
                    return True
        return False

    def wallsAndGates(self, rooms):
        # Time Complexity O(m*n)
        # Runtime: 328 ms, faster than 42.47%
        # Memory Usage: 16.8 MB, less than 55.65%
        q = []
        for i in range(len(rooms)):
            for j in range(len(rooms[i])):
                if rooms[i][j] == 0:
                    q.append([i,j])

        depth = 1
        tmp = []
        while q:
            room = q.pop(0)
            x,y = room
            for pt in [ [x+1, y], [x-1, y], [x, y+1], [x, y-1] ]:
                if self.is_valid(pt, rooms):
                    tmp.append(pt)
                    rooms[pt[0]][pt[1]] = depth

            if not q:
                depth += 1
                q = tmp
                tmp = []

        return rooms

s = Solution()
print s.wallsAndGates(
[[2147483647,-1,0,2147483647],
[2147483647,2147483647,2147483647,-1],
[2147483647,-1,2147483647,-1],
[0,-1,2147483647,2147483647]
])
# [[3,-1,0,1],[2,2,1,-1],[1,-1,2,-1],[0,-1,3,4]]

