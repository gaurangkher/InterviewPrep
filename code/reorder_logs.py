class Solution(object):
    def reorderLogFiles(self, logs):
        # Time Commplexity O(nlog(n)

        nums = []
        letters = []
        for log in logs:
            (key, line) = log.split(' ',  1)
            if line[0].isalpha():
                letters.append(log)
            else:
                nums.append(log)

        def sorter(x):
            (key, line) = x.split(' ',  1)
            return (line, key)
        letters = sorted(letters, key=sorter)

        letters.extend(nums)

        return letters


s = Solution()
s.reorderLogFiles(["dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"])
# ["let1 art can","let3 art zero","let2 own kit dig","dig1 8 1 5 1","dig2 3 6"]

