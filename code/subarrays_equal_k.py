# python2
class Solution(object):
    def subarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """

        start = 0
        count = 0
        total_sum = 0
        d = {}
        for end in range(len(nums)):
            total_sum += nums[end]
            if total_sum - k == 0 and not d.has_key(total_sum - k):
                d[0] = 1
            if d.has_key(total_sum - k):
                count += d[total_sum-k]
            if not d.has_key(total_sum):
                d[total_sum] = 1
            else:
                d[total_sum] += 1

            #print 'I %s count %s d %s' % (end, count, d)
        return count


s = Solution()
print s.subarraySum([0,0,0,0], 0)
print s.subarraySum([1,2,3], 3)
print s.subarraySum([-1, -1,1], 0)
