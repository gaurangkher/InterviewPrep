# python
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def __str__(self):
        ret = '%s' % self.val
        if self.next:
            ret = ret + ' -> %s' % str(self.next)
        return ret


class Solution(object):

    def mergeTwoListsRecursion(self, l1, l2):
        # Recursion
        # Time Complexity O(n)
        # Runtime: 400 ms, faster than 10.26%
        # Memory Usage: 32.8 MB, less than 5.04%
        if not l1 and not l2:
            return
        if not l1:
            return l2
        if not l2:
            return l1
        r = ListNode()
        if l1.val < l2.val:
            r.val = l1.val
            l1 = l1.next
        else:
            r.val = l2.val
            l2 = l2.next
        r.next = self.mergeTwoLists(l1, l2)
        return r

    def mergeTwoLists(self, l1, l2):
        # Simple merge
        # Python root pointer ref is tricky dont return dummy
        # Time Complexity O(n)
        # Runtime: 64 ms, faster than 5.95%
        # Memory Usage: 12.8 MB, less than 37.36%
        if not l1 and not l2:
            return
        if not l1:
            return l2
        if not l2:
            return l1
        r = ListNode()
        root = r

        while l1 is not None and l2 is not None:
            if l1.val < l2.val:
                r.next = ListNode(l1.val)
                l1 = l1.next
            else:
                r.next = ListNode(l2.val)
                l2 = l2.next
            r = r.next

        while l1:
            r.next = ListNode(l1.val)
            r = r.next
            l1 = l1.next
        while l2:
            r.next = ListNode(l2.val)
            l2 = l2.next
            r = r.next
        return root.next



from lib.utils import build_linked_list

l1 = build_linked_list([1,2,4], cls=ListNode)
l2 = build_linked_list([1,3,4], cls=ListNode)
s = Solution()
print s.mergeTwoLists(l1, l2)
print s.mergeTwoListsRecursion(l1, l2)

l1 = build_linked_list([1], cls=ListNode)
print s.mergeTwoLists(l1, None)
print s.mergeTwoListsRecursion(l1, None)
