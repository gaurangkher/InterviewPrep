# python 2
class Solution():
    def numSubarrayProductLessThanK(self, nums, k):
        # Time Complexity O(n)
        # Sliding Window 
        # Count all matched window and current at same time
        # Runtime: 1340 ms, faster than 20.35% 
        # Memory Usage: 16.6 MB, less than 70.00%
        start = 0
        tsum = 0
        ret = 0
        for end in range(len(nums)):
            tsum += nums[end]
            while tsum >= k:
                tsum -= nums[start]
                start += 1
            ret += (end - start) + 1

        return ret

s = Solution()
print s.numSubarrayProductLessThanK([2, 5, 6], 10)
print s.numSubarrayProductLessThanK([1, 11, 2, 3, 15], 10)
print s.numSubarrayProductLessThanK([1,1,1], 1)
print s.numSubarrayProductLessThanK([1,1,1], 2)
