# python 2
class Trie():
    def __init__(self):
        self.dict = {}
        self.words = []

    def __repr__(self):
        vals = []
        for k in self.dict.keys():
            vals.append((self.dict[k]))
        return str({
            'keys': self.dict.keys(),
            'words': self.words,
            'tries': vals,
        })

class Solution():
    # Trie Solution
    # Runtime: 284 ms, faster than 45.36% 
    # Memory Usage: 22.4 MB, less than 21.15%
    # Time Complexity: O(m*n) m = length of prodcuts array, n = length of each product
    # Insert takes m*n time, we ignore sort since we only sort upto 4 words. 
    # Search takes O(n) we can inore this too since n << m, so Complexity is O(m*n)

    def suggestedProducts(self, products, searchWord):
        trie = Trie()
        # add Trie
        for word in products:
            node = trie
            for c in word:
                if not node.dict.has_key(c):
                    node.dict[c] = Trie()
                node = node.dict[c]
                node.words.append(word)
                node.words.sort()
                if len(node.words) > 3:
                    # We need at most 3 words only
                    node.words = node.words[:3]
        # search
        results = []
        node = trie
        mismatch = False
        for c in searchWord:
            if c in node.dict.keys() and not mismatch:
                node = node.dict[c]
                results.append(node.words)
            else:
                mismatch = True
                results.append([])

        return results

s = Solution()
print s.suggestedProducts(["mobile","mouse","moneypot","monitor","mousepad", "mount","monstor"], 'mouse')
print s.suggestedProducts(["havana"], 'havana')
print s.suggestedProducts(["bags","baggage","banner","box","cloths"], 'bags')
print s.suggestedProducts(["havana"], 'tatiana')
