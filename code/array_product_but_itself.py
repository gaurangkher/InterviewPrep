#python2
class Solution(object):
    def productExceptSelf(self, nums):
        # With division, consider 0
        # O(2n) ~ O(n)
        is_zero = False
        product = 1
        atleast_one = False
        # Get cumulative product, skip zero elements
        for n in nums:
            if n:
                atleast_one = True
                product *= int(n)
            else:
                is_zero = True

        if not atleast_one:
            product = 0
        # Divide by cumulative product
        array = []
        for n in nums:
            if is_zero:
                if n:
                    array.append(0)
                else:
                    array.append(product)
            else:
                array.append(product/n)
        return array

    def productExceptSelfnoDiv(self, nums):
        # No Division, Use left product and right product
        # Runtime: 112 ms, faster than 49.47%
        # Memory Usage: 21.9 MB, less than 13.94%
        # O(n)
        lpa = 1
        rpa = 1
        out = [1]*len(nums)
        for i in range(0,len(nums)):
            if i < len(nums)-1:
                lpa *= nums[i]
                out[i+1] *= lpa
            j = len(nums)-1-i
            if j > 0:
                rpa *= nums[j]
                out[j-1] *= rpa
        return out

# Tests
s = Solution()
print s.productExceptSelf([1,2,3,4]) # [24,12,8,6]
print s.productExceptSelf([1,2,3,0,4]) # [0, 0, 0, 24, 0]

print s.productExceptSelfnoDiv([1,2,3,4])
print s.productExceptSelfnoDiv([1,2,0,3,4])
