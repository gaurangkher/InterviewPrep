# python2
class Solution(object):
    def sortColors(self, nums):
        # 3 pointers , left for 0, right for 2, curr for currrent
        # Time Complexity O(n), Space O(1)
        # Runtime: 24 ms, faster than 60.94%
        # Memory Usage: 12.7 MB, less than 77.34%
	left = 0
        curr = 0
        right = len(nums) - 1
        while curr <= right:
            if nums[curr] == 2:
                nums[curr], nums[right] = nums[right], nums[curr]
                right -= 1
            elif nums[curr] == 0:
                nums[curr], nums[left] = nums[left], nums[curr]
                left += 1
                curr += 1
            else:
                curr += 1
            #print 'L%s C%s R%s : %s' % (left, curr, right, nums)

        return nums

s = Solution()
print s.sortColors([2,0,2,1,1,0]) # [0,0,1,1,2,2]
