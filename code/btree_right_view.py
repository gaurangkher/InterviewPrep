# python2
# Definition for a binary tree node.
#class TreeNode(object):
#    def __init__(self, val=0, left=None, right=None):
#        self.val = val
#        self.left = left
#        self.right = right

class Solution(object):
    def right_only(self, node, depth, path):
        if depth == len(path):
            path.append(node.val)

        if node.right:
            path = self.right_only(node.right, depth+1, path)
        if node.left:
            path = self.right_only(node.left, depth +1, path)
        return path

    def rightSideView(self, root):
        # Time Complexity is O(n)
        # Runtime: 32 ms, faster than 23.93% 
        # Memory Usage: 12.7 MB, less than 74.07%
        if not root:
            return None
        return self.right_only(root, 0, [])

from lib.utils import TreeNode, build_tree

t = [1,2,3,None,5,None,4]
tree = build_tree(t)
s = Solution()
print s.rightSideView(tree) # [1,3,4] 
