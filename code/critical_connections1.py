# python2
class Node():
    def __init__(self, value):
        self.value = value
        self.children = []
        self.parent = None
        self.low = -1
        self.visited = False

class Graph():
    def __init__(self):
        self.nodes = []
    def add_node(self, edge):
        n1 = Node(edge[0])
        n2 = Node(edge[1])
        self.nodes.append()
            
class Solution():
    def criticalConnections(self, n, connections):
        # DFS
        pass

s = Solution()
# 3 -- 4 -- 2
#  \  /
#    1 -- 2
print s.criticalConnections(5, [[1, 2], [1, 3], [3, 4], [1, 4], [4, 5]]) # [1, 4]

# 1    4
# | \ /  \
# |  2    6
# | / \  /
# 3    5
print s.criticalConnections(6, [[1, 2], [1, 3], [2, 3], [2, 4], [2, 5], [4, 6], [5, 6]]) # [2]

#    1        7
#   / \      / \
#  2 - 3 -- 6   8
#      |     \ /
#      4-- 5  9

print s.criticalConnections(9, [[1, 2], [1, 3], [2, 3], [3, 4], [3, 6], [4, 5], [6, 7], [6, 9], [7, 8], [8, 9]])
# [3,4,6]

