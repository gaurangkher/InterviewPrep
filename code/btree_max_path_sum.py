# python 2
class Solution(object):
    def __init__(self):
        self.max_cnt = None

    def max_count(self, node, count):
        if not node:
            return count

        l = self.max_count(node.left, count)
        r = self.max_count(node.right, count)
        local_sum = node.val
        if l:
            local_sum = max(local_sum, local_sum+l)

        if r:
            local_sum = max(local_sum, node.val+r)

        if self.max_cnt is None:
            self.max_cnt = node.val
        self.max_cnt = max(local_sum, self.max_cnt)
        if l and r:
            self.max_cnt = max(self.max_cnt, l+r+node.val)

        return local_sum

    def maxPathSum(self, root):
        # DFS
        # Time Complexity O(N)
        # Runtime: 80 ms, faster than 89.67% 
        # Memory Usage: 25.2 MB, less than 77.78%
        if not root:
            return 0
        count = None
        self.max_count(root, count)
        return self.max_cnt

from lib.utils import build_tree
s = Solution()
print s.maxPathSum(build_tree([1,2,3])) # 6
print s.maxPathSum(build_tree([-10,9,20,None,None,15,7])) # 42

