# python 2
class Solution(object):
    def totalFruit(self, tree):
        # Time Complexity is O(n)
        # Runtime: 976 ms, faster than 40.64%
        # Memory Usage: 17.3 MB, less than 90.82% 
        mlen = 0
        d = {}
        j = 0
        for i, t in enumerate(tree):
            if d.has_key(t):
                d[t] += 1
            else:
                d[t] = 1
            while len(d.keys()) > 2:
                d[tree[j]] -= 1
                if d[tree[j]] == 0:
                    del d[tree[j]]
                j += 1
            mlen = max(mlen, sum(d.values()))

        return mlen


s = Solution()
print s.totalFruit([3,3,3,1,2,1,1,2,3,3,4]) # 5
print s.totalFruit([1,0,1,4,1,4,1,2,3]) # 5
