# python2
from collections import defaultdict
class Solution(object):
    def ladderLength(self, beginWord, endWord, wordList):
        # Time Complexity O(M^2 * N) , M = length of word, N is total no of words
        # Runtime: 120 ms, faster than 75.32%
        # Memory Usage: 17.8 MB, less than 19.68%
        L = len(beginWord)

        all_combo_dict = defaultdict(list)
        for word in wordList:
            for i in range(L):
                all_combo_dict[word[:i] + "*" + word[i+1:]].append(word)

        q = [beginWord]
        new_q = []
        depth = 1
        visited = {}
        while q:
            w = q.pop(0)
            if w == endWord:
                return depth

            for i in range(L):
                comb = w[:i] + "*" + w[i+1:]
                if all_combo_dict.has_key(comb):
                    for w1 in all_combo_dict[comb]:
                        if not visited.has_key(w1):
                            new_q.append(w1)
                            visited[w1] = 1

            if not q:
                q = new_q
                new_q = []
                depth += 1

        return 0

s = Solution()
print s.ladderLength('hit', 'cog', ["hot","dot","dog","lot","log","cog"]) # 5
