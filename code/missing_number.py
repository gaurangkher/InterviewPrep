class Solution(object):
    def missingNumber(self, nums):
        # Time Complexity O(n)
        # Runtime: 140 ms, faster than 37.38%
        # Memory Usage: 15.7 MB, less than 5.10%
        d = {}
        max_n = 0
        for n in nums:
            d[n] = 1
            max_n = max(max_n,n)

        for x in range(max_n+1):
            if not d.has_key(x):
                return x
        return max_n+1

s = Solution()
print s.missingNumber([3,0,1]) # 2
print s.missingNumber([0,1]) # 2

