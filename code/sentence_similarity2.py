# python 2
class Node():
    def __init__(self, x):
        self.val = x
        self.parent = x
        self.rank = 0
class UnionFind():
    def __init__(self, pairs):
        self.d = {}
        self.pairs = pairs

    def find(self, x):
        if x.parent != x.val:
            x.parent = self.find(self.d[x.parent])
        return x.parent

    def initialize(self):
        for p in self.pairs:
            k = p[0]
            if not self.d.has_key(k):
                self.d[k] = Node(k)
            v = p[1]
            if not self.d.has_key(v):
                self.d[v] = Node(v)
                self.d[v].parent = k
                self.d[v].rank += 1
            else:
                # Merge
                parent = self.find(self.d[v])
                dp = self.find(self.d[k])
                self.d[dp].parent = parent
                self.d[dp].rank += 1

    def union(self, x, y):
        if not self.d.has_key(x) or not self.d.has_key(y):
            return False
        x_parent = self.d[self.find(self.d[x])]
        y_parent = self.d[self.find(self.d[y])]
        if x_parent.val == y_parent.val:
            if x_parent.rank < y_parent.rank:
                self.d[x].parent = y_parent.val
                self.d[x].rank += 1
            else:
                self.d[y].parent = x_parent.val
                self.d[y].rank += 1
            return True
        return False


class Solution(object):

    def areSentencesSimilarTwo(self, words1, words2, pairs):
        # Time Complexity O(Nlog(P)) , N is length of words, P is number of words in pars
        # Runtime: 464 ms, faster than 51.03%
        # Memory Usage: 14.8 MB, less than 84.83%
        if len(words1) != len(words2):
            return False
        uf = UnionFind(pairs)
        uf.initialize()

        for i in range(len(words1)):
            w1 = words1[i]
            w2 = words2[i]
            if w1 == w2 or uf.union(w1,w2):
                pass
            else:
                return False
        return True


s = Solution()
print s.areSentencesSimilarTwo(
["great","acting","skills"],
["fine","drama","talent"],
[["great","good"],["fine","good"],["drama","acting"],["skills","talent"]]
)
# True
