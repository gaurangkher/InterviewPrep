# python2
class Solution(object):
    def __init__(self):
        self.found = False
    def dfs_find(self, root, val):
        if not root:
            return False
        if root.val == val:
            return True
        if root.val < val:
            return self.dfs_find(root.right, val)
        else:
            return self.dfs_find(root.left, val)


    def dfs(self, node, root2, target):
        if self.found:
            return True
        if not node:
            return False

        if self.dfs_find(root2, target - node.val):
            self.found = True
            return self.found

        f = self.dfs(node.left, root2, target)
        if f:
            return True
        f = self.dfs(node.right, root2, target)
        return f

    def twoSumBSTs(self, root1, root2, target):
        # Time Complexity O(nlog(m))
        # Runtime: 164 ms, faster than 20.51%
        # Memory Usage: 19.9 MB, less than 53.84%
        if not root1 or not root2:
            return False
        return self.dfs(root1, root2, target)

from lib.utils import build_tree
s = Solution()
print s.twoSumBSTs(build_tree([2,1,4]), build_tree([1,0,3]), 5) # True
print s.twoSumBSTs(build_tree([0,-1,10]), build_tree([5,1,7,0,2]), 18) # False

