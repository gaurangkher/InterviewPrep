# python2 
class Solution(object):
    def dfs(self, node, p, q):
        if node.val == p.val or node.val == q.val:
            return node
        if (p.val < node.val and q.val > node.val):
            return node
    
        if p.val < node.val  and p.val < node.val and node.left:
            return self.dfs(node.left, p, q)
        if p.val > node.val and q.val > node.val and node.right:
            return self.dfs(node.right, p, q)
        
    def lowestCommonAncestor(self, root, p, q):
        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        if q.val < p.val:
            p,q = q,p
        return self.dfs(root, p, q)

from lib.utils import build_tree, TreeNode

t = build_tree([6,2,8,0,4,7,9,None,None,3,5])
s = Solution()
print s.lowestCommonAncestor(t, TreeNode(2), TreeNode(8)).val # 6
