# python 2
class Solution(object):
    def invertTree(self, root):
	# Time Complexity O(n)
        # Runtime: 16 ms, faster than 86.96%
	# Memory Usage: 12.7 MB, less than 63.43%
	if not root:
            return
        t = root.left
        root.left = root.right
        root.right = t
        self.invertTree(root.left)
        self.invertTree(root.right)
        return root

from lib.utils import build_tree
s = Solution()
t = build_tree([4,2,7,1,3,6,9])
print s.invertTree(t) # ['4', '7', '2', '9', '6', '3', '1']
