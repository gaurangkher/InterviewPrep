# python2
class Solution(object):
    def arrayRankTransform(self, arr):
        # Time Complexity O(nlog(n))
        # Runtime: 336 ms, faster than 85.58% 
        # Memory Usage: 33.4 MB, less than 17.31% 
        if not arr:
            return []
        sorted_arr = sorted(arr)
        #print sorted_arr
        d = {}
        i = 1
        prev = sorted_arr[0]
        d[prev] = i
        for j in range(1,len(sorted_arr)):
            x = sorted_arr[j]
            if prev == x:
                pass
            else:
                i += 1
                prev = x
            d[x] = i

        #print d
        ret = []
        for x in arr:
            ret.append(d[x])
        return ret

s = Solution()
print s.arrayRankTransform([40,10,20,30]) # [4,1,2,3]

