class Buffer():
    def __init__(self, capacity):
        self.capacity = capacity
        self.q = []

    def _q_get(self, n):
        ret = []
        while self.q and n:
            ret.append(self.q.pop(0))
            n = n - 1
        return ret

    def read(self, n):
        return self._q_get(n)

    def _q_put(self, arr):
        count = 0
        for x in arr:
            if len(self.q) < self.capacity:
                self.q.append(x)
                count += 1
            else:
                break
        return count

    def write(self, src):
        return self._q_put(src)

buf = Buffer(5)
print buf.write(['a','b','c'])
print buf.write(['d','e','f'])
print buf.q
print buf.read(3)
buf.write(['x', 'y', 'z','a','b','c'])
print buf.read(8)
