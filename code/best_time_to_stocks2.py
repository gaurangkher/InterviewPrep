# python2
class Solution(object):
    def maxProfit(self, prices):
        # Since any number of trades is allowed , we can just play incrementals
        # Time Complexity is O(n)
        # Runtime: 40 ms, faster than 98.28%
        # Memory Usage: 13.6 MB, less than 94.83%
        max_profits = 0
        if not prices:
            return max_profits

        for i in range(1, len(prices)):
            if prices[i] > prices[i-1]:
                max_profits = max_profits + (prices[i] - prices[i-1])

        return max_profits

s = Solution()
print s.maxProfit([7,1,5,3,6,4]) #7


