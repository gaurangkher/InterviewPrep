class Solution(object):
    def prisonAfterNDays(self, cells, N):
        # O(1)
        # Trick is pattern repeats after 14 times, + 14 is to make sure iters is not 0
        # Instead of iterrs it could be xrange(N)
        current = cells
        iters = (N % 14 ) + 14
        for i in xrange(iters):
            new_state = []
            for index, val in enumerate(current):
                if index == 0 or index == len(current)-1:
                    new_state.append(0)
                elif current[index - 1] == current[index + 1]:
                    new_state.append(1)
                else:
                    new_state.append(0)
            current = new_state

        return current

#TODO: Below isnt optinal enough for LeetCode
class CacheSolution(object):
    def prisonAfterNDays(self, cells, N):
        # O(1)
        # Save result in dict
        current = cells
        cache = {}
        for i in xrange(N):
            new_state = []
            if cache.has_key(''.join(current)):
                current = cache[''.join(current)]
            else:
                for index, val in enumerate(current):
                    if index == 0 or index == len(current)-1:
                        new_state.append(0)
                    elif current[index - 1] == current[index + 1]:
                        new_state.append(1)
                    else:
                        new_state.append(0)
                cache[''.join(current)] = new_state
                current = new_state

        return current

s = Solution()
s.prisonAfterNDays([0,1,0,1,1,0,0,1], 7) # [0,0,1,1,0,0,0,0]

s.prisonAfterNDays([1,0,0,1,0,0,1,0], 1000000000) # [0,0,1,1,1,1,1,0]
