# python2
# Given a non-empty list of words, return the k most frequent elements.
import heapq

class Word():
    def __init__(self, word, count):
        self.word = word
        self.count = count
    def __cmp__(self, obj):
        if self.count == obj.count:
            if self.word > obj.word:
                return -1
            else:
                return 1
        else:
            return cmp(self.count, obj.count)

class Solution():
    def topKFrequent(self, words, k):
	# Time Complexity n + nlog(k) = O(nlog(k))
	# Runtime: 52 ms, faster than 50.00%
	# Memory Usage: 12.7 MB, less than 95.28%
        d = {}
        for w in words:
            if d.has_key(w):
                d[w] += 1
            else:
                d[w] = 1

        arr = []
        for key in d.keys():
            heapq.heappush(arr, Word(key, d[key]))
            if len(arr) > k:
                heapq.heappop(arr)

        new_arr = [heapq.heappop(arr).word for x in range(k)]
        new_arr.reverse()
        return new_arr

    def topKFrequentnln(self, words, k):
	# Time Complexity O(nlog(n))
	# Runtime: 40 ms, faster than 91.89%
	# Memory Usage: 12.9 MB, less than 37.63%
        d = {}
        for w in words:
            if d.has_key(w):
                d[w] += 1
            else:
                d[w] = 1

        arr = []
        for key in d.keys():
            arr.append(key)

        def key_eval(k):
            return (-d[k], k)

        def comparator(a,b):
            if d[a] == d[b]:
                if a < b:
                    return 1
                return -1

            return cmp(d[a],d[b])

        #arr = sorted(arr, cmp=comparator)
        #arr.reverse()
        arr = sorted(arr, key=key_eval)
        #print arr
        return arr[0:k]

s = Solution()
print s.topKFrequent(['aaa', 'a', 'a'], 1) #
print s.topKFrequent(["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], 4)

