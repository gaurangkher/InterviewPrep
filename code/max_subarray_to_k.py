#python 2

class Solution:
    # Complexity: O(n)
    # Runtime: 104 ms, faster than 61.40%
    # Memory Usage: 15.2 MB, less than 67.16% 
    def maxSubArrayLen(self, nums, k):
        # Store cumulative sum as key and index as value and look for difference
        cum_sum = 0
        length = 0
        cum_dict = {}
        for i in xrange(len(nums)):
            cum_sum += nums[i]
            # IF cumulative sum has gone up more than k, what is the offset
            # Because place where cum sum was = offset, is where we start counting
            # to get to sum k
            # WE save all cumulative sums in a dictionary
            extra = cum_sum - k

            # If cumulative sum comes from index 0
            if extra == 0:
                length = max(length, i+1)
                cum_dict[cum_sum] = i
            else:
                if cum_dict.has_key(extra):
                    if i - cum_dict[extra] > length:
                        length = i - cum_dict[extra]
                # Only store if we dont have, that way cum_dict always has min start index
                if not cum_dict.has_key(cum_sum):
                    cum_dict[cum_sum] = i

        return length


s = Solution()
print s.maxSubArrayLen([1, -1, 5, -2, 3], 3) # 4
print s.maxSubArrayLen([-2, -1, 2, 1], 1) # 2
print s.maxSubArrayLen([1,0,-1], -1) # 2
#print test([1,0,-1], -1)
print s.maxSubArrayLen([0,0,0], 0)
