# python 2
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def dfs(self, node, el, stack):

        if not node:
            return stack

        stack.append(node)
        if node.val == el.val:
            return stack
        for n in [node.left, node.right]:
            if n is not None:
                stack = self.dfs(n, el, stack)

                if len(stack) >= 1 and stack[-1].val == el.val:
                    return stack

        if stack:
            stack.pop()
        return stack


    def lowestCommonAncestor(self, root, p, q):
        # DFS for both  p and q and the compare paths
        # Time Complexity O(n)
        # Runtime: 116 ms, faster than 14.81% 
        # Memory Usage: 26.4 MB, less than 32.54%
        path1= self.dfs(root, p, [])
        path2 = self.dfs(root, q, [])
        common = None

        for i in range(min(len(path1), len(path2))):
            if path1[i].val != path2[i].val:
                break
            else:
                common = path1[i]

        return common


from lib.utils import TreeNode, build_tree

s = Solution()
n = s.lowestCommonAncestor(build_tree([3,5,1,6,2,0,8,None,None,7,4]), build_tree([5]), build_tree([1]))
print n.val # 3

n = s.lowestCommonAncestor(build_tree([3,5,1,6,2,0,8,None,None,7,4]), build_tree([5]), build_tree([4]))
print n.val # 5
