# python2
class Solution(object):
    def maxProfit(self, prices):
        # DP, Assume we have two sub-divisions for 2 chances
        # left profits calculates all possible profits seen from left
        # The next sub-division would be right to the left,
        # So we calculate all right profits
        # Time Complecity is O(n)
        # Runtime: 88 ms, faster than 35.73%
        # Memory Usage: 14 MB, less than 49.74%
        if not prices:
            return 0
        max_profit = 0
        l_p = [0]
        min_p = prices[0]
        for x in range(1, len(prices)):
            min_p = min(prices[x], min_p)
            p = prices[x] - min_p
            max_profit = max(p, max_profit)
            l_p.append(max_profit)

        max_profit = 0
        max_p = prices[-1]
        r_p = [0]*len(prices)
        for x in range(len(prices) - 2, -1, -1):

            p = max_p - prices[x]
            max_profit = max(p, max_profit)
            r_p[x] = max_profit
            max_p = max(prices[x], max_p)

        max_profit = 0
        for x in range(1, len(prices)-2):
            p = l_p[x] + r_p[x+1]
            max_profit = max(max_profit, p)
        max_profit = max(max_profit, l_p[-1])
        return max_profit

s = Solution()
print s.maxProfit([6,1,3,2,4,7]) # 7
print s.maxProfit([3,3,5,0,0,3,1,4]) # 6
print s.maxProfit([1,2,3,4,5]) # 4
print s.maxProfit([7,6,4,3,1]) # 0
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
