# python 2
class Solution(object):
    def is_substring(self, source, i, target, j):
        si = i
        ti = j
        while si < len(source) and ti < len(target):
            if source[si] == target[ti]:
                ti += 1
            si += 1
        return ti

    def shortestWay(self, source, target):
        # Time Complecity O(N*M) 
        # N is length of target, M is length of source
        # Runtime: 48 ms, faster than 51.16%
        # Memory Usage: 12.7 MB, less than 88.37% 
        d = {}
        for i in range(len(source)):
            if not d.has_key(source[i]):
                d[source[i]] = i
        count = 0
        i = 0
        while i < len(target):
            c = target[i]
            if d.has_key(c):
                i1 = self.is_substring(source, d[c] , target, i)
                count += 1
                i = i1 - 1 # Account for +1 later
            else:
                return -1
            i += 1

        if i == len(target):
            return count
        return -1

s = Solution()
print s.shortestWay('abc', 'abcbc') # 2
print s.shortestWay('xyz', 'xzyxz') # 3
