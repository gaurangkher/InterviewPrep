#python2
class Solution(object):
    def searchRange(self, nums, target):
        # Binary Search to find leftmost first
        # Another binary search to find rightmost
        # Time Complexity O(log(n))
        # Runtime: 72 ms, faster than 72.42%
        # Memory Usage: 14 MB, less than 73.33% 

        if len(nums) < 2:
            if target in nums:
                return [0,0]
            return [-1, -1]

        res = [-1, -1]
        # Find leftmost target
        start = 0
        end = len(nums) - 1
        while start <= end:
            if start == end and nums[start] == target:
                res[0] = start
                break
            else:
                mid = start + ((end - start) / 2)
                if nums[mid] < target:
                    start = mid + 1
                elif nums[mid] > target:
                    end = mid - 1
                else:
                    end = mid

        if res[0] == -1:
            return res

        # Find rightmost target
        start = res[0]
        end = len(nums) - 1
        while start <= end:
            if start == end and nums[start] == target:
                res[1] = start
                break
            else:
                mid = start + ((end - start) / 2) + 1
                if nums[mid] < target:
                    start = mid + 1
                elif nums[mid] > target:
                    end = mid - 1
                else:
                    start = mid

        return res
