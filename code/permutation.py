# python2 
class Solution(object):
    def __init__(self):
        self.result = []

    def backtrack(self, nums, count):
        if count == len(nums):
            # Need to send copy of array
            # Otherwise all elements are same at return
            self.result.append(nums[:])
            return
        for i in range(count, len(nums)):
            # Swap
            num2 = nums
            num2[count], num2[i] = num2[i], num2[count]
            self.backtrack(num2, count + 1)
            # Swap back
            num2[count], num2[i] = num2[i], num2[count]

    def permute(self, nums):
        # Backtracking
        # backtrack to rebuild array with different combination
        # Time Complexity is O(n*n!) 
        # n! because we those are the permutations, n because we copy array each time
        # Runtime: 24 ms, faster than 92.53%
        # Memory Usage: 12.8 MB, less than 52.51%
        self.backtrack(nums, 0)
        return self.result

s = Solution()
print s.permute([1,2,3]) # [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 2, 1], [3, 1, 2]]
