#python2
class Solution(object):
    def __init__(self):
        self.cache = {}
    def is_palindrome(self, s):
        if self.cache.has_key(s):
            return self.cache[s]
        start = 0
        end = len(s) - 1
        while start < end:
            if s[start] == s[end]:
                start += 1
                end -= 1
                self.cache[s] = self.is_palindrome(s[start:end+1])
                return self.cache[s]
            else:
                self.cache[s] = False
                return self.cache[s]
        self.cache[s] = True
        return self.cache[s]

    def longestPalindrome(self, s):
        # Dynamic Programming
        # Time Complexity O(n^2)
        # We do better than simple brute force O(n^3)
        # Because of caching results. is_palindrome otherwise takes O(n)
        # TLE for leetcode
        if len(s) <= 1:
            return s

        longest = ''
        for x in range(len(s)):
            for y in range(x+1, len(s)+1):
                #print 'X %s y %s' % (x, y)
                s1 = s[x:y]
                if len(s1) <= len(longest):
                    continue
                if self.is_palindrome(s1):
                    if len(s1) > len(longest):
                        longest = s1

        return longest

    def expand(self, start, end, s):
        if s[start] != s[end]:
            return ''
        while start >= 0 and end < len(s) and s[start] == s[end]:
            start -= 1
            end += 1
        return s[start+1:end]

    def longestPalindromeExpand(self, s):
        # Assume We are at center of palinndrome and expand out to get palindrome
        # Time Complexity is still O(n^2)
        # Runtime: 772 ms, faster than 85.32%
        # Memory Usage: 13 MB, less than 23.94%
        if len(s) == 1:
            return s
        longest = ''
        for x in range(len(s)-1):
            #print 'X %s' % x
            ret = self.expand(x, x, s) # Assume center of odd palindrome
            #print 'O ret %s' % ret
            if len(ret) > len(longest):
                longest = ret
            ret = self.expand(x, x+1, s) # Assume center of even palindrome
            #print 'E ret %s' % ret
            if len(ret) > len(longest):
                longest = ret

        return longest

s = Solution()
t1 = "ukxidnpsdfwieixhjnannbmtppviyppjgbsludrzdleeiydzawnfmiiztsjqqqnthwinsqnrhfjxtklvbozkaeetmblqbxbugxycrlzizthtuwxlmgfjokhqjyukrftvfwikxlptydybmmzdhworzlaeztwsjyqnshggxdsjrzazphugckgykzhqkdrleaueuajjdpgagwtueoyybzanrvrgevolwssvqimgzpkxehnunycmlnetfaflhusauopyizbcpntywntadciopanyjoamoyexaxulzrktneytynmheigspgyhkelxgwplizyszcwdixzgxzgxiawstbnpjezxinyowmqsysazgwxpthloegxvezsxcvorzquzdtfcvckjpewowazuaynfpxsxrihsfswrmuvluwbdazmcealapulnahgdxxycizeqelesvshkgpavihywwlhdfopmmbwegibxhluantulnccqieyrbjjqtlgkpfezpxmlwpyohdyftzgbeoioquxpnrwrgzlhtlgyfwxtqcgkzcuuwagmlvgiwrhnredtulxudrmepbunyamssrfwyvgabbcfzzjayccvvwxzbfgeglqmuogqmhkjebehtwnmxotjwjszvrvpfpafwomlyqsgnysydfdlbbltlwugtapwgfnsiqxcnmdlrxoodkhaaaiioqglgeyuxqefdxbqbgbltrxcnihfwnzevvtkkvtejtecqyhqwjnnwfrzptzhdnmvsjnnsnixovnotugpzuymkjplctzqbfkdbeinvtgdpcbvzrmxdqthgorpaimpsaenmnyuyoqjqqrtcwiejutafyqmfauufwripmpcoknzyphratopyuadgsfrsrqkfwkdlvuzyepsiolpxkbijqw" # aueua
t2 = "babad" # bab
t3 = "bb" # bb
print s.longestPalindrome(t1) 
print s.longestPalindrome(t2)
print s.longestPalindrome(t3)

print s.longestPalindromeExpand(t1)
print s.longestPalindromeExpand(t2)
print s.longestPalindromeExpand(t3)
