class Solution():
    def nextPermutation(self, num):
        # Time Complexity O(n)
        # Runtime: 52 ms, faster than 7.23% 
        # Memory Usage: 12.8 MB, less than 38.15%
        # Find 1st descending number from right
        # Trick : 
        # Find the first decreasing number from right to left as first_desc
        # Find next greater number than first_desc to its right
        # Swap first_desc and next greater 
        # Reverse list after first_desc to end
        if len(num) < 2:
            return num
        first_desc = len(num) - 1
        found = False
        for index in range(len(num)-2, -1, -1):
            if num[index] < num[index + 1]:
                first_desc = index
                found = True
                break

        if not found:
            # Reverse full
            start = 0
            end = len(num) -1
            index = 0
            while index <= ((end - start) // 2):
                self.swap(start + index, end - index, num)
                index += 1
        else:
            # find next greater
            index = first_desc + 1
            greater = index
            while index < len(num):
                if num[index] > num[first_desc]:
                    if num[index] <= num[greater]:
                        greater = index
                index += 1
            # Swap
            self.swap(first_desc, greater, num)
            first_desc += 1
            # reverse
            start = first_desc
            end = len(num) -1
            index = 0
            while index <= ((end - start) // 2):
                self.swap(start + index, end - index, num)
                index += 1
        return num

    def swap(self, x, y, num):
        # Swap
        temp = num[x]
        num[x] = num[y]
        num[y] = temp




s = Solution()
print s.nextPermutation([1,0,6,3])
print s.nextPermutation([1,0,6,0,3])
print s.nextPermutation([1,2,3])
print s.nextPermutation([5,4,7,5,3,2]) # [5,5,2,3,4,7]
print s.nextPermutation([1,3,2])
print s.nextPermutation([2,3,1,3,3])
print s.nextPermutation([1,5,1])
