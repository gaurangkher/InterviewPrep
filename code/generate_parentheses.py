# python2
class Solution(object):
    def __init__(self):
        self.result = []
        self.n = None
    def backtrack(self, string, left, right):
        if len(string) == 2*self.n:
            self.result.append(string)
            return

        if left < self.n:
            self.backtrack(string+'(', left + 1, right)
        if right < left:
            self.backtrack(string + ')', left, right + 1)

    def generateParenthesis(self, n):
        # Backtracking (because we only recurse on specific condition)
        # Time Complexity is Catalan Number (1/n+1)*(2n choose n)
        # = (1/n+1)*(2n!/n!*(2n-n)!)
        # Runtime: 20 ms, faster than 89.10%
        # Memory Usage: 12.8 MB, less than 83.02%
        self.n = n
        self.backtrack('', 0, 0)
        return self.result

s = Solution()
print s.generateParenthesis(3) # ['((()))', '(()())', '(())()', '()(())', '()()()']
