# python2
class Solution(object):
    def rob(self, nums):
        # dp[i] = max(dp[i-2] + arr[i], dp[i-1])
        # Time Complexity O(n)
        # Runtime: 16 ms, faster than 92.31%
        # Memory Usage: 12.6 MB, less than 86.33% 
        minus_two = 0
        minus_one =  0
        max_sum = 0
        for i in range(len(nums)):
            max_sum = max(minus_two + nums[i], minus_one)
            minus_two, minus_one = minus_one, max_sum
        return max_sum

s = Solution()
print s.rob([2,1,1,2])
print s.rob([1,2,3,1]) # 4
print s.rob([2,1,1,2,1,2]) # 6
