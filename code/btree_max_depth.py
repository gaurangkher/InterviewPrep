# python 2
class Solution(object):
    # Time Complexity O(n)
    # Runtime: 36 ms, faster than 39.72%
    # Memory Usage: 15.8 MB, less than 22.82%
    def dfs(self, node, depth, max_depth):
        if not node:
            return max_depth
        ld = self.dfs(node.left, depth+1, max_depth)
        rd = self.dfs(node.right, depth+1, max_depth)
        return max(depth, ld, rd, max_depth)
    
    def maxDepth(self, root):
        # Time Complexity 
        if not root:
            return 0
        return self.dfs(root, 1, 1)

from lib.utils import build_tree
s = Solution()
print s.maxDepth(build_tree([3,9,20,None,None,15,7])) # 3
