# python 2
# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
    def __repr__(self):
        return '%s -> %s' % (self.val, str(self.next))
class Solution(object):
    def removeNthFromEnd(self, head, n):
        # Linked List Remove, set pointer correctly
        # Boundry Condition is important
        # Runtime: 16 ms, faster than 97.12% 
        # Memory Usage: 12.9 MB, less than 12.61%
        p = head
        curr = head
        for i in range(n):
            curr = curr.next

        if not curr:
            if p.next:
                head = p.next
                return head
            else:
                return None
        while curr.next:
            curr = curr.next
            p = p.next


        nth = p.next
        p.next = nth.next

        return head

from lib.utils import build_linked_list
s = Solution()
ll = build_linked_list([1,2,3,4,5], cls=ListNode)
print s.removeNthFromEnd(ll, 2) # 1->2->3->5->None
