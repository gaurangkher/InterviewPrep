# python 2
class Solution():
    # Time Complexity : O(nlog(n))
    # Runtime: 72 ms, faster than 71.97% 
    # Memory Usage: 14.9 MB, less than 81.08%
    def merge(self, intervals):
        if len(intervals) < 2:
            return intervals
        def sorter(x):
            return x[0]
        sorted_intervals = sorted(intervals, key=sorter)

        merged_intervals = [sorted_intervals.pop(0)]
        for interval in sorted_intervals:
            if interval[0] <= merged_intervals[-1][1]:
                merged_intervals[-1][1] = max(merged_intervals[-1][1], interval[1])
            else:
                merged_intervals.append(interval)

        return merged_intervals



