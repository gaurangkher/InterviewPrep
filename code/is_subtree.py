# python 2
class Solution(object):
    def dfs(self, s, t):
        if not s and not t:
            return True
        if not s or not t:
            return False
        if s.val == t.val and self.dfs(s.left, t.left) and self.dfs(s.right, t.right):
            return True
        return False
    
    def isSubtree(self, s, t):
	# Time Complexity O(S*T)
        # Runtime: 296 ms, faster than 44.92%
        # Memory Usage: 13.9 MB, less than 53.18%) 
        if not s and not t:
            return True
        if not s or not t:
            return False
        if s.val == t.val:
            found = self.dfs(s,t)
            if found:
                return found
        found = self.isSubtree(s.left, t)
        if found:
            return found
        found = self.isSubtree(s.right, t)
        return found

from lib.utils import build_tree
s = build_tree([3,4,5,1,2])
t = build_tree([4,1,2])
sol = Solution()
print sol.isSubtree(s,t) # True

