import heapq


class Heap():
    def __init__(self, arr, min_heap=True):
        self.heap = arr
        self.min_heap = min_heap
        self._heapify()

    def heapify(self):
        #TODO: Implement this
        pass

    def push(self, element):
        pass

    def pop(self):
        return element


class Solution():

    def connectSticks(A):
        #  O(nlog(n))
        # Heap
        heapq.heapify(A)
        res = 0
        while len(A) > 1:
            x = heapq.heappop(A)
            y = heapq.heappop(A)
            res += x + y
            heapq.heappush(A, x + y)
        return res


print connectSticks([20, 4, 8, 2])
