# python 2
class Solution(object):
    def licenseKeyFormatting(self, S, K):
        # Time Complexity O(n)
        # Runtime: 404 ms, faster than 44.62%
        # Memory Usage: 15.2 MB, less than 37.57%
	i = 0
        ret = ''
        for index in range(len(S)-1,-1,-1):
            c = S[index]
            if c.isalnum():
                if i == K:
                    i = 1
                    ret = '%s-%s' % (c.upper(), ret)
                else:
                    i += 1
                    ret = '%s%s' % (c.upper(), ret)

        return ret

s = Solution()
print s.licenseKeyFormatting("2-5g-3-J", 2) # "2-5G-3J"
