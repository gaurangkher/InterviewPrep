from threading import Semaphore
class BoundedBlockingQueue(object):
    def __init__(self, capacity):
        # Runtime: 620 ms, faster than 5.74%
	# Memory Usage: 13.9 MB, less than 27.05%
	self.q = []
        self.capacity = capacity
        self._sem = Semaphore()


    def enqueue(self, element):
        """
        :type element: int
        :rtype: void
        """
        while len(self.q) > self.capacity:
            pass
        self._sem.acquire()
        self.q.append(element)
        self._sem.release()
        return

    def dequeue(self):
        """
        :rtype: int
        """
        while not self.q:
            pass
        self._sem.acquire()
        e = self.q.pop(0)
        self._sem.release()
        return e

    def size(self):
        """
        :rtype: int
        """
        self._sem.acquire()
        s = len(self.q)
        self._sem.release()
        return s

