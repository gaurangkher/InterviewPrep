# python2
class Solution(object):
    def calculateTime(self, keyboard, word):
        # Time Complexity O(n)
	# Runtime: 32 ms, faster than 86.18%
	# Memory Usage: 13 MB, less than 17.51%
	i = 0
        d = {}
        for c in keyboard:
            d[c] = i
            i += 1

        count = 0
        index = 0
        for c in word:
            c_index = d[c]
            count += abs(index-c_index)
            index = c_index

        return count

s = Solution()
print s.calculateTime("abcdefghijklmnopqrstuvwxyz", "cba") # 4
print s.calculateTime("pqrstuvwxyzabcdefghijklmno", "leetcode") # 73
