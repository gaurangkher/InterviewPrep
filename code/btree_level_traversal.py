# python 2
class Solution(object):
    def levelOrder(self, root):
        # BFS
        # Time Complexity O(n)
        # Runtime: 24 ms, faster than 73.53% 
        # Memory Usage: 13.2 MB, less than 87.70%
        if not root:
            return []
        q = [root]
        result = []
        children = []
        depth = []
        while q:
            node = q.pop(0)
            depth.append(node.val)

            if node.left:
                children.append(node.left)
            if node.right:
                children.append(node.right)

            if not q:
                q = children
                result.append(depth)
                depth = []
                children = []

        return result

from lib.utils import build_tree
t = build_tree([3,9,20,None,None,15,7])
s = Solution()
print s.levelOrder(t) # [[3], [9, 20], [15, 7]] 

