= Google = 
:google:

== Process ==
* [X] Recruiter Contact
* [ ] Tech Phone Screen
* [ ] Virtual Onsite 5 interviews 

== Recent Phone Screens ==
* [X] https://leetcode.com/discuss/interview-question/704810/Google-or-Phone-or-Longest-Subarray-Midpoint
* https://leetcode.com/discuss/interview-question/700536/Google-or-Phone-or-Valid-Date
* [X] https://leetcode.com/discuss/interview-question/698670/Google-or-Phone-screen-or-Sentence-similarity-(variant)-June-2020
* https://leetcode.com/discuss/interview-question/681909/Google-or-Phone-or-Image-Dimensions
* https://leetcode.com/discuss/interview-question/680992/Google-or-Phone-or-Solve-series-of-arithemetic-equations
* https://leetcode.com/discuss/interview-question/679162/Google-or-Phone-or-Find-Index-of-Extra-Character
* https://leetcode.com/discuss/interview-question/672160/Google-or-Phone-or-Street-Fighter
* https://leetcode.com/discuss/interview-question/671856/Google-or-Phone-or-Pick-Random-Country
* https://leetcode.com/discuss/interview-question/668896/Google-or-Phone-or-Group-odd-valued-and-even-valued-nodes-in-Linked-List
* https://leetcode.com/discuss/interview-question/668450/Google-or-Phone-or-Construct-Matrix
* https://leetcode.com/discuss/interview-question/667176/Google-or-Phone-or-Forest-of-Sub-Trees
* https://leetcode.com/discuss/interview-question/666766/Google-or-Phone-or-Please-help-how-to-solve-problem
* https://leetcode.com/discuss/interview-question/656131/Google-or-Phone-or-Unit-Test-Calculator
* https://leetcode.com/discuss/interview-question/799477/Google-or-Phone-Screen-or-Reject
* https://leetcode.com/discuss/interview-question/746360/Google-or-Phone-or-Implement-TCP-stream
* https://leetcode.com/discuss/interview-question/736717/Google-or-Phone-or-Start-to-End-with-Safe-states
* https://leetcode.com/discuss/interview-question/727705/Google-or-Phone-or-Given-N-sorted-arrays-find-k-smallest-elements
* https://leetcode.com/discuss/interview-question/723230/Google-or-Phone-or-Kth-missing-number
* https://leetcode.com/discuss/interview-question/707842/Google-or-Phone-or-Time-to-reach-message-to-all-the-employees
* https://leetcode.com/discuss/interview-question/707835/Google-or-Phone-or-Find-origin-of-malware-transmission
* https://leetcode.com/discuss/interview-question/799477/Google-or-Phone-Screen-or-Reject
* https://leetcode.com/discuss/interview-question/653576/Google-or-Phone-or-Common-substrings-between-two-strings
* https://leetcode.com/discuss/interview-question/643158/Google-or-Phone-or-Faulty-Keyboard
* https://leetcode.com/discuss/interview-question/630341/Google-or-Phone-Screen-or-Pick-a-random-Number
* https://leetcode.com/discuss/interview-question/629293/Google-or-Phone-or-Lakes-and-Dragons
* https://leetcode.com/discuss/interview-question/621249/Google-or-Phone-or-Fix-Broken-Binary-Tree
* https://leetcode.com/discuss/interview-question/619724/Google-or-Phone-or-Generate-Jewel-Crush-Board
* https://leetcode.com/discuss/interview-question/615526/Google-or-Phone-or-Print-BST-leaves-in-alternate-order
* https://leetcode.com/discuss/interview-question/609750/Google-or-Phone-or-Optimal-Account-Balancing
* https://leetcode.com/discuss/interview-question/608094/Google-or-Phone-or-String-to-Integer
* https://leetcode.com/discuss/interview-question/603504/Google-or-Phone-or-Bounding-box-of-connected-component
* https://leetcode.com/discuss/interview-question/601397/Google-or-Phone-or-Minimum-Routers-to-Cover-an-Array

== Related ==
==== related google ====
