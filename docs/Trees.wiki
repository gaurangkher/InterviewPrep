= Trees =
:tree:

{{{
class tree_node()
    def __init__():
        self.value
        # For multi-children trees
        self.children
        # For binary tree, left right tree
        self.left 
        self.right

class Tree():
    def __init__(self):
        pass
    def add_node(self, node):
        return
    def traverse(self):
        # dfs, bfs
        # inorder, preorder, postorder
        return
    def delete_node(self, node):
        return
}}}

== Types ==
* [[Heap]]
* [[Binary Search Tree]]
* 
== Algorithms ==
* [[BFS]]
* [[DFS]]
* [[Union-Find]]
* [[Backtracking]]
* [[ALpha-Beta Pruning]]
* 

=== Gotchas ===
* Dont force fit trees into MAtrix problems if they use DFS (eg: [[problems/islands|Number of Islands]] Problem)

