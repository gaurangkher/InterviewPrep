= Breadth First Search =
:bfs:

* Traverse Layer by Layer
* Use Queue 
  
== Iterative ==
{{{
import Queue
def iterate(tree):
    # To measure depth, use another queue for children 
    # Drain the main queue and then substitute with the other one.
    # To just iterate following is good enough
    if not tree:
        return
    q = Queue()
    q.put(tree)
    while not q.empty():
        node = q.get()
        #Process
        for c in node.children:
            q.put(c)
            
}}}

== Recursive ==
{{{
def recurse(q):
    if not q:
        return
    node = q.get()
    # Process Node
    for c in node.children():
        q.put(c)
    recurse(q)
}}}


=== Notes ===
* BFS for depth without extra space: {{https://stackoverflow.com/questions/31247634/how-to-keep-track-of-depth-in-breadth-first-search|Link}}
* Python Queue is thread safe, but does not have a peek method
* Python array can be used a Queue (put->append, get->pop(0))


== Related Problems ==
==== related bfs ====
