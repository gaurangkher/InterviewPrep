= String Manipulation =
:string:

== Problem Types and Techniques==

| Type       | Description                                        | Techniques                                                                |
|------------|----------------------------------------------------|---------------------------------------------------------------------------|
| Anagram    | Words formed by recombining other words            | sort, hash                                                                |
| Palindrome | Words that read same forward or backward           | reverse, two pointers [[Manchar]]                                         |
| Searching  | Character search, substring search, replace,concat | suffix array, sliding window, [[KMP]] [[Z-Algo]] [[Trie]]  [[Rabin-Karp]] |



== Algorithms and Data Structures ==
* [[KMP]]
* [[Z-Algo]]
* [[Manchar]]



== Problems ==

| Name | Hint | Algorithm/Technique |
|------|------|---------------------|
|      |      |                     |

