= Resources =
:resources:

== Design Patterns ==
:design-patterns:

- https://refactoring.guru/design-patterns


== Websites ==
:websites:

* Leetcode - https://leetcode.com/problemset/all/
* CodeChef - https://www.codechef.com/
* Geeks for Geeks - https://www.geeksforgeeks.org/
* Start - https://yangshun.github.io/tech-interview-handbook/algorithms/array
* Prep MAterial - https://dev.to/seattledataguy/the-interview-study-guide-for-software-engineers-764
* Educative (System Design and other resources) - https://www.educative.io 
* Prep InterviewBit - https://www.interviewbit.com/courses/system-design/topics/storage-scalability/
* Prep Geeks4Geeks - https://practice.geeksforgeeks.org/explore/?company%5B%5D=Amazon&problemType=functional&page=1&sortBy=submissions
* 
== Books ==
:books:

* Programming Interviews Exposed 
  - Admin Code: eL2gSfeynrVde8v0KbtB
  - https://gofile.io/d/Vzh9vB
* Cracking the Coding Interview
  - Admin Code: itXjGOpeMoxjsjDkZM0p
  - https://gofile.io/d/93bVSC

== Top Questions ==
* https://www.teamblind.com/post/New-Year-Gift---Curated-List-of-Top-75-LeetCode-Questions-to-Save-Your-Time-OaM1orEU
