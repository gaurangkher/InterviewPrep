%title Top K Frequently Mentioned Keywords
= Top K Frequently Mentioned Keywords = 
:medium:hash:trie:heap:amazon:lexicographic-ordering:bst:practice:

=== Ref: ===
* https://leetcode.com/discuss/interview-question/542597/
* https://leetcode.com/problems/top-k-frequent-words/
* https://leetcode.com/problems/top-k-frequent-elements/
* https://www.geeksforgeeks.org/find-the-k-most-frequent-words-from-a-file/

== Problem ==
Given a list of reviews, a list of keywords and an integer k. Find the most popular k keywords in order of most to least frequently mentioned.

The comparison of strings is case-insensitive.
Multiple occurances of a keyword in a review should be considred as a single mention.
If keywords are mentioned an equal number of times in reviews, sort alphabetically.

Eg:
{{{
Input:
k = 2
keywords = ["anacell", "betacellular", "cetracular", "deltacellular", "eurocell"]
reviews = [
  "I love anacell Best services; Best services provided by anacell",
  "betacellular has great services",
  "deltacellular provides much better services than betacellular",
  "cetracular is worse than anacell",
  "Betacellular is better than deltacellular.",
]

Output:
["betacellular", "anacell"]

Explanation:
"betacellular" is occuring in 3 different reviews. "anacell" and "deltacellular" are occuring in 2 reviews, but "anacell" is lexicographically smaller.
}}}

=== Hint: ===
* Hash and Sort
* Min Heap

== Solution ==
{{../../code/top_k_freq_words.py}}

Slight variation of problem 
With heap and without heap
{{../../code/top_k_freq_words1.py}}

== Run ==
