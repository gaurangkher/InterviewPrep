= Move Zeroes =
:easy:array:swap:two-pointer:practice:

=== Ref ===
https://leetcode.com/problems/move-zeroes/

== Problem ==
Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
Note:
1: Do this In-Place
2: Minimize operations

Eg:
{{{
Input: [0,1,0,3,12]
Output: [1,3,12,0,0]
}}}

=== Hint ===
* Two pointers

== Solution ==
{{../../code/move_zeroes.py}}

== Related ==
==== related two-pointer ../ ====

== Run ==
