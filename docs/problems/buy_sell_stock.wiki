%title Best Time to Buy and Sell Stock
= Best Time to Buy and Sell Stock =
:easy:DP:amazon:facebook:microsoft:apple:google:practice:

=== Ref ===
* https://leetcode.com/problems/best-time-to-buy-and-sell-stock/


== Problem ==
Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.

Note that you cannot sell a stock before you buy one.

Eg:
{{{
Input: [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
             Not 7-1 = 6, as selling price needs to be larger than buying price.

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.
}}}


=== Hint ===
* Keep local minima and max profit

== Solution ==
{{../../code/best_time_to_stocks.py}}

== Related ==
==== related DP ../ ====

== Run ==
