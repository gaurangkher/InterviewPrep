%title Prison Cells After N Days
= Prison Cells After N days =
:hard:trick:amazon:bit-manipulation:hash:bits:amazon:

=== Ref: ===
* https://leetcode.com/problems/prison-cells-after-n-days/

== Problem ==
There are 8 prison cells in a row, and each cell is either occupied or vacant.
Each day, whether the cell is occupied or vacant changes according to the following rules:
If a cell has two adjacent neighbors that are both occupied or both vacant, then the cell becomes occupied.
Otherwise, it becomes vacant.

(Note that because the prison is a row, the first and the last cells in the row can't have two adjacent neighbors.)

 Given the initial state of the prison, return the state of the prison after N days (and N such changes described above.)

Eg:
{{{
Input: cells = [0,1,0,1,1,0,0,1], N = 7
Output: [0,0,1,1,0,0,0,0]
Explanation: 
The following table summarizes the state of the prison on each day:
Day 0: [0, 1, 0, 1, 1, 0, 0, 1]
Day 1: [0, 1, 1, 0, 0, 0, 0, 0]
Day 2: [0, 0, 0, 0, 1, 1, 1, 0]
Day 3: [0, 1, 1, 0, 0, 1, 0, 0]
Day 4: [0, 0, 0, 0, 0, 1, 0, 0]
Day 5: [0, 1, 1, 1, 0, 1, 0, 0]
Day 6: [0, 0, 1, 0, 1, 1, 0, 0]
Day 7: [0, 0, 1, 1, 0, 0, 0, 0]


Input: cells = [1,0,0,1,0,0,1,0], N = 1000000000
Output: [0,0,1,1,1,1,1,0]
}}}

=== Hint ===
* For a large N runtime goes too long
* Pattern repeats after 14 times
* Bit Manipulation
* Store output has hash
* 
== Solution ==
{{../../code/prison_cells_n_days.py}}


== Run ==
