%title Reverse Integer
= Reverse Integer =
:easy:stack:amazon:facebook:apple:microsoft:

=== Ref ===
* https://leetcode.com/problems/reverse-integer/

== Problem ==
Given a 32-bit signed integer, reverse digits of an integer. Return 0 if result overflows 32bit signed int.
Eg:
{{{
Input: 123
Output: 321

Input: -123
Output: -321

Input: 120
Output: 21
}}}

=== Hint ===
* Stack

== Solution ==
{{../../code/reverse_integer.py}}

== Related ==
==== related stack ../ ====

== Run ==
