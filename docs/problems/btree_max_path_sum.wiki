%title BTree Maximum Path Sum
= Binary Tree Maximum Path Sum =
:hard:dfs:trick:facebook:amazon:google:microsoft:apple:practice:

=== Ref ===
* https://leetcode.com/problems/binary-tree-maximum-path-sum/

== Problem ==
Given a non-empty binary tree, find the maximum path sum.

For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree along the parent-child connections. The path must contain at least one node and does not need to go through the root.
Eg:
{{{
Input: [1,2,3]

       1
      / \
     2   3

Output: 6

Input: [-10,9,20,null,null,15,7]

   -10
   / \
  9  20
    /  \
   15   7

Output: 42
}}}

=== Hint ===
* DFS
* Return max path for node but keep total_max sum seperate


== Solution ==
{{../../code/btree_max_path_sum.py}}

== Related ==
==== related dfs ../ ====

== Run ==
