%title Shortest Word Distance
= Shortest Word Distance =
:easy:string:amazon:google:

=== Ref ===
* https://leetcode.com/problems/shortest-word-distance/

== Problem ==
Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.
Eg:
{{{
words = ["practice", "makes", "perfect", "coding", "makes"]
Input: word1 = “coding”, word2 = “practice”
Output: 3

Input: word1 = "makes", word2 = "coding"
Output: 1
}}}

You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.


=== Hint ===
* Linear time

== Solution ==
{{../../code/shortest_word_distance.py}}

== Related ==
==== related string ../ ====

== Run ==
