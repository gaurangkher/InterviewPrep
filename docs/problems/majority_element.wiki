%title Majority Element
= Majority Element =
:easy:hash:sort:divide-and-conquer:amazon:google:apple:microsoft:

=== Ref ===
* https://leetcode.com/problems/majority-element/solution/
* [[https://en.wikipedia.org/wiki/Boyer%E2%80%93Moore_majority_vote_algorithm|Boyer-Moore Voting Algorithm]]

== Problem ==
Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

You may assume that the array is non-empty and the majority element always exist in the array.

Eg:
{{{
Input: [3,2,3]
Output: 3

Input: [2,2,1,1,1,2,2]
Output: 2
}}}

=== Hint ===
Options 
* Hash
* Sort
* Randomization
* Divide and Conquer
* Boyer-Moore Voting

== Solution ==
{{../../code/majority_element.py}}

== Related ==
==== related hash ../ ====

== Run ==
