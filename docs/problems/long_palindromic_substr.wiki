%title Longest Palindromic SubString
= Longest Palindromic SubString =
:medium:DP:amazon:microsoft:amazon:facebook:apple:linkedin:uber:

=== Ref ===
* https://leetcode.com/problems/longest-palindromic-subsequence/

== Problem ==
Given a string s, find the longest palindromic subsequence's length in s. You may assume that the maximum length of s is 1000.

Eg:
{{{
Input:  "bbbab"
Output: 4
Explanation: longest palindromic subsequence is "bbbb". 

Input: "cbbd"
Output: 2
Explanation: longest palindromic subsequence is "bb". 
}}}


=== Hint ===
* Brute Force + DP using caching
* Expand Around Center
* [[../Manacher's Algorithm|Manacher's Algorithm]]


== Solution ==
*todo* TODO: Manacher's
{{../../code/longest_palindrome.py}}

== Related ==
==== related DP ../ ====

== Run ==
