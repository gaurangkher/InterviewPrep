%title Trim BST
= Trim BST =
:easy:tree:dfs:recursion:google:facebook:apple:microsoft:amazon:practice:

=== Ref ===
* https://leetcode.com/problems/trim-a-binary-search-tree/

== Problem ==
Given the root of a binary search tree and the lowest and highest boundaries as low and high, trim the tree so that all its elements lies in [low, high]. You might need to change the root of the tree, so the result should return the new root of the trimmed binary search tree.
Eg:
{{{
Input: root = [1,0,2], low = 1, high = 2
Output: [1,null,2]

Input: root = [3,0,4,null,2,null,null,1], low = 1, high = 3
Output: [3,2,null,1]
       3                3
      / \              /
     0   4   ->       2 
      \              /
       2            1
      /
     1

}}}

=== Hint ===
* DFS
* recursion

== Solution ==
{{../../code/trim_bst.py}}

== Related ==
==== related tree ../ ====

== Run ==
