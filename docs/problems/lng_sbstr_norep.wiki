%title Longest Substring Without Repeating Characters
= Longest Substring Without Repeating Characters =
:medium:string:sliding-window:trick:

=== Ref ===
* https://leetcode.com/problems/longest-substring-without-repeating-characters/

== Prolem ==
Given a string s, find the length of the longest substring without repeating characters.
Eg:
{{{
Input: s = "abcabcbb"
Output: 3

Input: s = "bbbbb"
Output: 1

Input: s = "pwwkew"
Output: 3
}}}

=== Hint ===
* sliding window
* Set start as max of exisitng or i+1

== Solution ==
{{../../code/longest_substring_wo_repeating_chars.py}}

== Related ==
==== related trick ../ ====

== Run ==
