%title Maximum SubArray sums to K
= Maximum SubArray Sums to k =
:medium:array:prefix-array:suffix-array:two-pointer:trick:practice:

=== Ref ===
* https://leetcode.com/problems/maximum-size-subarray-sum-equals-k/

== Problem ==
Given an array nums and a target value k, find the maximum length of a subarray that sums to k. If there isn't one, return 0 instead. (MAx sum is a 32-bit int)

Eg:
{{{
Input: nums = [1, -1, 5, -2, 3], k = 3
Output: 4 
Explanation: The subarray [1, -1, 5, -2] sums to 3 and is the longest.

Input: nums = [-2, -1, 2, 1], k = 1
Output: 2 
Explanation: The subarray [-1, 2] sums to 1 and is the longest.

[1,2,5,-5,3]
}}}

Follow Up:
Can you do it in O(n) time?

=== Hint ===
* Use hash

== Solution ==
* Linear Time
{{../../code/max_subarray_to_k.py}}

== Related ==
==== related trick ../ ====

== Run ==
