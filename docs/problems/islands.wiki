%title Number of Islands
= Numer of Islands =
:matrix:dfs:disjoint-set:graph:undirected-graph:connected-component:union-find:amazon:ebay:facebook:microsoft:google:ebay:uber:visa:linkedin:doordash:snapchat:arista:lyft:twitter:atlassian:twitch:paypal:nvidia:

=== Ref ===
* [[https://en.wikipedia.org/wiki/Component_(graph_theory)|Connected Component]]
* [[https://www.geeksforgeeks.org/find-number-of-islands/|Geeks4Geeks BFS]]
* [[https://www.geeksforgeeks.org/find-the-number-of-islands-set-2-using-disjoint-set/|Geeks4Geeks Disjoint Set]]
* https://leetcode.com/problems/number-of-islands/ 

== Problem ==
Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. 
An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. 
You may assume all four edges of the grid are all surrounded by water.

Eg1:
{{{Input:
11110
11010
11000
00000

Output: 1
}}}
Eg2:
{{{Input:
11000
11000
00100
00011

Output: 3
}}}

=== Hint: ===
1: DFS
2: Disjointed Set

=== Remember: ===
DONT Spend time converting to tree

== Solution ==
* DFS
{{../../code/islands.py}}

* Union Find
{{../../code/islands1.py}}

== Related ==
==== related union-find ../ ====

== Run ==
