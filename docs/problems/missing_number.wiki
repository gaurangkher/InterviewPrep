%title Missing Number
= Missing Number =
:easy:sort:hash:amazon:apple:facebook:microsoft:ebay:google:

=== Ref ===
* https://leetcode.com/problems/missing-number/

== Problem ==
Given an array nums containing n distinct numbers in the range [0, n], return the only number in the range that is missing from the array.

Follow up: Could you implement a solution using only O(1) extra space complexity and O(n) runtime complexity?
Eg:
{{{
Input: nums = [3,0,1]
Output: 2
Explanation: n = 3 since there are 3 numbers, so all numbers are in the range [0,3]. 2 is the missing number in the range since it does not appear in nums.
}}}

=== Hint ===
* For space complexity O(1), use index as property 0 < i < n

== Solution ==
{{../../code/missing_number.py}}

== Related ==
==== related hash ../ ====

== Run ==
