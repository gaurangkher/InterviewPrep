%title Best Time to Buy and Sell Stock 3
= Best Time to Buy and Sell Stock 3 =
:hard:DP:trick:amazon:

=== Ref ===
* https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/

== Problem ==
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most two transactions.

Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).

Eg:
{{{
Input: [3,3,5,0,0,3,1,4]
Output: 6
Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
             Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.
             
Input: [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
             Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
             engaging multiple transactions at the same time. You must sell before buying again.
             
Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.
}}}

=== Hint ===
* Trick , consider left and right partition and calculate left side and right side profits (see solution)
* DP

== Solution ==
{{../../code/best_time_to_stocks3.py}}

== Related ==
==== related DP ../ ====

== Run ==
