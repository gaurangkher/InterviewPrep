%title Distinct Islands
= Distinct Islands =
:medium:dfs:amazon:facebook:union-find:

=== Ref ===
* https://leetcode.com/problems/number-of-distinct-islands/

== Problem ==
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

Count the number of distinct islands. An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.

Eg:
{{{
11000
11000
00011
00011
Output: 1

11011
10000
00001
11011
Output: 3
}}}

No rotation/reflection allowed

=== Hint ===
* DFS
* Sae path signnatures
* Union Find

== Solution ==
{{../../code/distinct_islands.py}}

== Related ==
==== related dfs ../ ====

== Run ==
