%title Is Subsequence
= Is SubSequence =
:easy:two-pointer:google:facebook:amazon:   

=== Ref ===
* https://leetcode.com/problems/is-subsequence/

== Problem ==
ven a string s and a string t, check if s is subsequence of t.

A subsequence of a string is a new string which is formed from the original string by deleting some (can be none) of the characters without disturbing the relative positions of the remaining characters. (ie, "ace" is a subsequence of "abcde" while "aec" is not).


=== Hint ===
* two pointer

== Solution ==
{{../../code/is_subsequence.py}}

== Related ==
==== related two-pointer ../ ====

== Run ==
